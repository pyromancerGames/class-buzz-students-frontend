angular.module('griffinStudents.home')

    .controller('HomeCtrl', ['$rootScope', '_', '$timeout', '$uibModal', 'authenticationServices', 'userServices', 'errorHandler', '$state',
        function ($rootScope, _, $timeout, $uibModal, authenticationServices, userServices, errorHandler, $state) {

            var viewModel = this;
            viewModel.openSortingModal = openSortingModal;
            viewModel.fullProfile = {};

            viewModel.userProfile = userServices.getUser(authenticationServices.getProfile().doc).
                then(function(user){
                    viewModel.fullProfile = user.data;
                    if (!user.data.student.house) {
                        openSortingModal();
                    } else {
                        $state.go('main.store');
                        return;
                    }
                })

            function openSortingModal(){
                //if (viewModel.fullProfile.student.house) return;

                $uibModal.open({
                    animation: true,
                    templateUrl: '/app/components/home/views/modals/spinnerModal.html',
                    backdrop: 'static',
                    keyboard: false,
                    controller: 'SpinnerCtrl'
                });
            }

    }])

    .controller('SpinnerCtrl', ['$scope','$rootScope', '$uibModalInstance', '_', 'authenticationServices', 'userServices', 'errorHandler', '$state',
        function ($scope, $rootScope, $uibModalInstance, _, authenticationServices, userServices, errorHandler, $state) {

            //var viewModel = this;
            $scope.selectedHouse = {};
            $scope.housesToSort = [];
            $scope.preAssignedHouse = {};
            $scope.closeModal = closeModal;
            $scope.canClose = false;

            userServices.getCustomerHouses(authenticationServices.getProfile())
                .then(function(houses){
                    $scope.housesToSort = houses.data;
                    $rootScope.$broadcast('housesToSortSet', houses.data);
                })

            userServices.getHouseToBeSortedTo(authenticationServices.getProfile())
                .then(function(house){
                    $scope.preAssignedHouse = house.data;
                    $rootScope.$broadcast('houseToBeSortedSet', house.data);
                })

            $rootScope.$on('usersorted', function(event, options){
                $scope.selectedHouse = options.house;
                userServices.updateHouse(authenticationServices.getProfile().doc, options.house._id);
                $scope.canClose = true;
            })

            function closeModal(){
                $uibModalInstance.dismiss('cancel');
                $state.go('main.store');
                return;
            }

        }]);
