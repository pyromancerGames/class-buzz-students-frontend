angular.module('griffinStudents.home')

        .directive('houseSpinner', function ($injector) {
            var linkFn = function (scope, ele, attrs) {

                // Build the game object
                var height = 500,
                    width = 400;

                var game = new Phaser.Game(
                    width,
                    height,
                    Phaser.AUTO,
                    'houseSpinner', {
                        preload: preload,
                        create: create,
                        update: update
                    },
                    true
                );

                game.scope = scope;

                function preload() {

                    this.load.crossOrigin = "*";
                    game.load.audio('click', '/app/public/assets/click.mp3');
                    game.load.audio('fanfare', '/app/public/assets/fanfare.mp3');

                    game.load.image('roulette', '/app/public/assets/spin-33.png');
                    game.load.image('spin', '/app/public/assets/spin-03.png');
                    game.load.image('stand', '/app/public/assets/spin-02.png');
                    game.load.image('arrow', '/app/public/assets/spin-05.png');
                }



                var canSpin = true;
                var houseIsSet = false;
                var houseToBeSortedIsSet = false;
                var sorted = false;
                var roulette, spinWheel, click, arrow;

                var houses = ['HOUSE1, HOUSE2, HOUSE3, HOUSE4'];
                var selectedHouse = {};
                var currentHouse = houses[0];

                //35 london bridge  - conwy castle
                //30 red bus        - blackrock
                //20 big ben        - stirling
                //25 london eye     - warwick

                scope.$on('housesToSortSet', function(event, data){
                    houses = data;
                    currentHouse = houses[0];
                    houseIsSet = true;
                });

                scope.$on('houseToBeSortedSet', function(event, data){
                    selectedHouse = data;
                    houseToBeSortedIsSet = true;
                });

                var slices = 8;

                function create() {

                    //stand = game.add.sprite(game.world.centerX, 100, 'stand');
                    //stand.anchor.setTo(0.5, 0);

                    roulette = game.add.sprite(game.world.centerX, game.world.centerY - 50, 'roulette');
                    roulette.anchor.setTo(0.5, 0.5);

                    spinWheel = game.add.sprite(game.world.centerX, game.world.centerY - 55, 'spin');
                    spinWheel.anchor.setTo(0.5, 0.5);

                    arrow = game.add.sprite(game.world.centerX - 100 , game.world.centerY + 30, 'arrow');
                    arrow.anchor.setTo(0.5, 0.5);
                    arrow.scale.setTo(0.7, 0.7);
                    arrow.angle = -35;
                    game.add.tween(arrow.scale).to( { x: 0.75, y: 0.75 }, 600, Phaser.Easing.Linear.None, true, 0, -1, true);

                    game.physics.startSystem(Phaser.Physics.P2JS);
                    game.physics.p2.enable(roulette);

                    roulette.body.setCircle(140);
                    roulette.body.angularDamping = 0.5;

                    game.input.onDown.add(doSpin, this);
                }

                function doSpin() {

                    if (canSpin && houseIsSet && houseToBeSortedIsSet) {
                        roulette.body.angularVelocity = selectedHouse.spinForce;
                        arrow.destroy();
                        canSpin = false;
                    }
                }

                function update() {

                    if (canSpin) return;
                    if (!houseIsSet) return;
                    if (!houseToBeSortedIsSet) return;
                    if (sorted) return;

                    var alpha = roulette.body.angle;


                    var pointerIndex = slices - 1 - Math.floor((alpha + 180) / (360 / slices));
                    pointerIndex = pointerIndex % houses.length;
                    var pointerHouse = houses[pointerIndex];

                    if (pointerHouse._id != currentHouse._id) {
                        currentHouse = pointerHouse;
                        game.add.audio('click').play();
                    }

                    if (roulette.body.angularVelocity <= 0.15) {
                        roulette.body.angularVelocity = 0;
                        scope.$emit('usersorted', {house:selectedHouse});
                        game.add.audio('fanfare').play();
                        canSpin = true;
                        sorted = true;
                    }

                }

            };

            return {
                restrict: 'E',
                scope: {
                    housesToSort: '='
                },
                template: '<div id="houseSpinner" class="spinner-center"></div>',
                link: linkFn
            }

        });

