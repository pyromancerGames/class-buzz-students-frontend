(function() {
    /**
    * @ngdoc function
    * @name griffinApp Error
    * @description
    * # Errors
    * Error handling module
    */
    angular.module('griffinStudents.errors', []);
})();