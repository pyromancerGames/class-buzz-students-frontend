(function () {
    /**
     * @ngdoc function
     * @name griffinApp.errors
     * @description
     * Services of the griffinApp error module
     */
    angular.module('griffinStudents.errors')

        .factory('errorHandler', [ '$rootScope', '$state', 'apiServices', 'authenticationServices', function ($rootScope, $state, apiServices, authenticationServices) {

            var ErrorHandler = {

                handleError: function (err, action) {

                    var loggedUser = authenticationServices.getProfile();

                    if (typeof action === 'undefinded') action = "Generic Action";

                    var e = {
                        statusCode: 500,
                        userMessage: err.message,
                        timestamp: Date.now(),
                        message: action,
                        user: loggedUser.id,
                        origin: "Students WEB",
                        platform: "Browser",
                        stack: err.stack
                    };

                    var req = {
                        method: 'POST',
                        url: $rootScope.gfConfig.APIEndpoint + '/api/v1/logger',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {err: e}
                    }

                    apiServices.apiRequest(req)
                        .then(function (response) {
                            console.log(response);
                            //$state.go('main.error');
                        })

                },

                /*
                 * AVAILABLE ERROR LEVELS
                 * info, warn, error, fatal
                 */

                notFoundError: function (errorMessage, userMessage) {
                    this.constructor.prototype.__proto__ = Error.prototype;
                    Error.captureStackTrace(this, this.constructor);
                    this.name = this.constructor.name;
                    this.message = errorMessage;
                    this.statusCode = 404;
                    this.level = 'info';
                    this.userMessage = typeof userMessage !== 'undefined' ? userMessage : 'An unexpected error has occurred';
                },

                notAuthorizedError: function (errorMessage, userMessage) {
                    this.constructor.prototype.__proto__ = Error.prototype;
                    Error.captureStackTrace(this, this.constructor);
                    this.name = this.constructor.name;
                    this.message = errorMessage;
                    this.statusCode = 401;
                    this.level = 'warn';
                    this.userMessage = typeof userMessage !== 'undefined' ? userMessage : 'An unexpected error has occurred';
                },

                genericCustomError: function (errorMessage, userMessage) {
                    this.constructor.prototype.__proto__ = Error.prototype;
                    Error.captureStackTrace(this, this.constructor);
                    this.name = this.constructor.name;
                    this.message = errorMessage;
                    this.statusCode = 401;
                    this.level = 'warn';
                    this.userMessage = typeof userMessage !== 'undefined' ? userMessage : 'An unexpected error has occurred';
                }

            }

            return ErrorHandler;

        }]);

})();