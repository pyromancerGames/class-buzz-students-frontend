angular.module('griffinStudents.liveFeed')

    .controller('LiveFeedCtrl', ['$rootScope', '$moment', 'authenticationServices', 'liveFeedServices', 'errorHandler',
        function ($rootScope, $moment, authenticationServices, liveFeedServices, errorHandler) {

            var viewModel = this;
            viewModel.userProfile = authenticationServices.getProfile();
            viewModel.liveFeedEvents = [];
            populateLiveFeed();

            $rootScope.$on('newevent', function(event, cbEvent){
                if(cbEvent.type != "Penalty") {
                    viewModel.liveFeedEvents.unshift(cbEvent);
                }
            });

            viewModel.fromNow = function(date){
                return($moment(date).fromNow());
            }

            function populateLiveFeed(){
                liveFeedServices.getCustomerEvents(viewModel.userProfile.customer)
                    .then( function(events) {
                        viewModel.liveFeedEvents = events.data;
                        console.log(events.data);
                    })
                    .catch(function(err){ errorHandler.handleError(err); })
            }

        }]);
