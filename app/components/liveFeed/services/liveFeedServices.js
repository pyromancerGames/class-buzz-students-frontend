(function () {
    angular.module('griffinStudents.liveFeed')

        .factory('liveFeedServices', ['$rootScope', '$state', 'apiServices', function ($rootScope, $state, apiServices) {

            var Services = {

                getCustomerEvents: function (customerId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/events/find/byCustomer';
                    var _data = {
                        customer: customerId,
                        type: "reward"
                    };

                    var req = {
                        method: 'POST',
                        url: _baseURL + _apiRoute,
                        headers: {},
                        data: _data
                    };

                    return apiServices.apiRequest(req);
                }


            }

            return Services;
        }]);

})();