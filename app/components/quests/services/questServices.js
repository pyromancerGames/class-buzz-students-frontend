(function () {
    angular.module('griffinStudents.quests')

        .factory('questServices', [ '$rootScope', '$state', 'apiServices', function ($rootScope, $state, apiServices) {

            var Services = {

                getUserQuests: function(userDoc){

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/users/' + userDoc + '/quest/achievements/all';

                    var req = {
                        method: 'GET',
                        url:  _baseURL + _apiRoute,
                        headers: {}
                    };

                    return apiServices.apiRequest(req);
                },

                getQuest: function(questId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/quest/' + questId;

                    var req = {
                        method: 'GET',
                        url:  _baseURL + _apiRoute,
                        headers: {}
                    };

                    return apiServices.apiRequest(req);
                }

            }

            return Services;
        }]);

})();