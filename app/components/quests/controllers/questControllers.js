angular.module('griffinStudents.quests')

    .controller('QuestCtrl', ['$rootScope', 'questServices', 'ngAudio','authenticationServices', 'achievementsServices','errorHandler', '_', '$sce',
        function ($rootScope, questServices, ngAudio, authenticationServices, achievementsServices, errorHandler, _, $sce) {

            var viewModel = this;
            viewModel.userProfile = authenticationServices.getProfile();
            viewModel.userQuests = {};
            viewModel.sounds = {
                newQuest: ngAudio.load("/app/public/sounds/fx/newQuest.wav"),
                questCompleted: ngAudio.load("/app/public/sounds/fx/questCompleted.wav")
            };

            populateUserQuests();

            $rootScope.$on('newquestavailable', function(){
                viewModel.newQuests += 1;
                viewModel.sounds.newQuest.play();
            });

            $rootScope.$on('questcompleted', function(event, quest){
                viewModel.sounds.questCompleted.play();
                viewModel.userQuests = _.reject(viewModel.userQuests, function(q){
                    return q._id == quest._id;
                });
            });

            function populateUserQuests() {
                questServices.getUserQuests(viewModel.userProfile.doc)
                    .then(function (quests) {

                        viewModel.userQuests = quests.data;

                        // Populate achievement quest info
                        for (var i = 0; i < viewModel.userQuests.length; i++) {

                            viewModel.userQuests[i].questInfo = '<div class="notification-pullup quest-info-tooltip">'
                                + viewModel.userQuests[i].quest.trigger.achievement.questInfo + '</div>';
                        }
                    })
                    .catch(function(err){ errorHandler.handleError(err, "Populating user quests"); })
            }

        }]);