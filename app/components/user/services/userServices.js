(function () {
    angular.module('griffinStudents.user')
        .factory('userServices', ['$localStorage', '$state', '$rootScope', '$location', '$q', 'authenticationServices', 'apiServices', 'errorHandler',
            function ($localStorage, $state, $rootScope, $location, $q , authenticationServices, apiServices, errorHandler) {

                var UserServices = {

                    getUser: function (userDocument) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userDocument;

                        var req = {
                            method: 'GET',
                            url: _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
                    },

                    updateUser: function (userProfile) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/update';

                        var req = {
                            method: 'POST',
                            url: _baseURL + _apiRoute,
                            headers: {},
                            data: userProfile
                        };

                        return apiServices.apiRequest(req);
                    },

                    updateHouse: function (userDocument, houseId) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userDocument + '/house/update';

                        var req = {
                            method: 'POST',
                            url: _baseURL + _apiRoute,
                            headers: {},
                            data: {id: houseId}
                        };

                        return apiServices.apiRequest(req);
                    },


                    getCustomerHouses: function (userProfile) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/houses/find/byCustomer';

                        var req = {
                            method: 'POST',
                            url: _baseURL + _apiRoute,
                            headers: {},
                            data: {customer: userProfile.customer}
                        };

                        return apiServices.apiRequest(req);
                    },


                    getHouseToBeSortedTo: function (userProfile) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/customers/'+ userProfile.customer +'/sorting/next';

                        var req = {
                            method: 'GET',
                            url: _baseURL + _apiRoute,
                            headers: {},
                        };

                        return apiServices.apiRequest(req);
                    },

                    getUserAchievements: function (userProfile) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userProfile.doc + '/character/achievements';

                        var req = {
                            method: 'GET',
                            url: _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
                    },

                    getUserNotifications: function(userProfile){
                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userProfile.id + '/messages';

                        var req = {
                            method: 'GET',
                            url:  _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
                    },
					
					updateUserAvatar: function(img) {
						
						var usr = {_id: authenticationServices.userProfile.id, avatarURL: img};
						
						var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/update';
						
                        var req = {
                            method: 'POST',
                            url: _baseURL + _apiRoute,
                            headers: {},
                            data: usr
                        };
						
						return apiServices.apiRequest(req);
					},

                    buyTrait: function(traitId, userDoc) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userDoc + '/trait/buy';

                        var req = {
                            method: 'POST',
                            url:  _baseURL + _apiRoute,
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                traitId: traitId
                            }
                        };

                        return apiServices.apiRequest(req);
                    },

					updateChar: function(char, userDoc) {

						var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userDoc + '/character/update';

                        var req = {
                            method: 'POST',
                            url:  _baseURL + _apiRoute,
                            headers: {
							'Content-Type': 'application/json'
							},
							data: char
                        };
						
                        return apiServices.apiRequest(req);
					},
					
					getUserTraitsInventory: function(userDoc) {
						
						var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + userDoc + '/invetory';
						
						var req = {
								method: 'GET',
								url:  _baseURL + _apiRoute,
								headers: {}
							};
	
							return apiServices.apiRequest(req);
					},

                    getUserCharacter: function(doc){
                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/' + doc + '/character';

                        var req = {
                            method: 'GET',
                            url:  _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
                    },


                    updatePassword: function(newPassword){
                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/users/password/update';

                        var req = {
                            method: 'POST',
                            url:  _baseURL + _apiRoute,
                            headers: {},
                            data: {password: newPassword}
                        };

                        return apiServices.apiRequest(req);
                    }

                }

                return UserServices;

            }]);

})();