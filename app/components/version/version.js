'use strict';

angular.module('griffinStudents.version', [
  'griffinStudents.version.interpolate-filter',
  'griffinStudents.version.version-directive'
])

.value('version', '0.1');
