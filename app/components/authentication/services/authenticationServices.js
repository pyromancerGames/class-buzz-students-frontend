'use strict';
(function () {
    /**
     * @ngdoc function
     * @name griffinApp.authentication
     * @description
     * Services of the griffinApp authentication
     */
    angular.module('griffinStudents.authentication')
        .factory('authenticationServices', ['$rootScope', '$state', '$localStorage', '$location', '_', 'jwtHelper',
            function ($rootScope, $state, $localStorage, $location, _, jwtHelper) {

                var AuthenticationServices = {

                    userToken: '',

                    userTokenLastUpdate: undefined,

                    userProfile: {},

                    getToken: function () {

                        if (AuthenticationServices.userToken) {
                            if (_.isEmpty(AuthenticationServices.userProfile)) {
                                AuthenticationServices.setProfile(AuthenticationServices.userToken);
                            }
                            return AuthenticationServices.userToken;
                        } else {
                            // Undefined if not exists
                            var newToken = $localStorage.gfAppUserToken;
                            if ($localStorage.gfAppUserToken) {
                                AuthenticationServices.userToken = newToken;
                                AuthenticationServices.setProfile(newToken);
                            }
                            return newToken;
                        }
                    },

                    logout: function () {
                        $localStorage.gfAppUserToken = '';
                        AuthenticationServices.userToken = '';
                        AuthenticationServices.userProfile = {};
                        $state.go('main.login');
                        return;
                    },

                    setProfile: function (token) {
                        // Set Profile
                        var usrProf = jwtHelper.decodeToken(token);
                        // Append may grant of union
                        usrProf = AuthenticationServices.setMayGrantPermissions(usrProf);
                        AuthenticationServices.userProfile = usrProf;
                        $rootScope.user = AuthenticationServices.userProfile;
                    },

                    getProfile: function () {
                        // Decode jwt and if not in memory decode from localstorage call setProfile and return
                        if (_.isEmpty(AuthenticationServices.userProfile)) {
                            if (AuthenticationServices.userIsLoggedIn()) {
                                AuthenticationServices.setProfile(AuthenticationServices.getToken());
                            } else {
                                // TODO: catch error
                                // TODO MSG
                                console.log("User not logged in or token unavailable");
                                return undefined;
                            }
                        }

                        return AuthenticationServices.userProfile;
                    },


                    setToken: function (newToken) {
                        AuthenticationServices.userToken = newToken;
                        $localStorage.gfAppUserToken = newToken;
                        AuthenticationServices.userTokenLastUpdate = new Date();
                    },

                    getMayGrantPermissions: function () {
                        if (!_.isEmpty(AuthenticationServices.userProfile)) {
                            return AuthenticationServices.userProfile.mayGrant;
                        } else {
                            return undefined;
                        }
                    },

                    setMayGrantPermissions: function (user) {

                        var mayGrant = [];

                        user.roles.forEach(function (r) {
                            mayGrant = _.union(mayGrant, r.mayGrant);
                        });

                        user.mayGrant = mayGrant;

                        return user;
                    },

                    getUserDoc: function () {

                        if (AuthenticationServices.userIsLoggedIn) {
                            return AuthenticationServices.userProfile.doc;
                        } else {
                            console.log("User not logged in while trying to get user");
                            return null;
                        }
                    },

                    getBranches: function () {
                        if (AuthenticationServices.userIsLoggedIn) {
                            return AuthenticationServices.userProfile.branches;
                        } else {
                            console.log("User not logged in while trying to get user branches");
                            return null;
                        }
                    },

                    userIsLoggedIn: function () {

                        if (AuthenticationServices.getToken()/* && !jwtHelper.isTokenExpired(AuthenticationServices.userToken)*/) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    getCustomer: function () {
                        if (AuthenticationServices.userIsLoggedIn) {
                            return AuthenticationServices.userProfile.customer;
                        } else {
                            console.log("User not logged in");
                            return undefined;
                        }
                    },

                    //Return may grant permission names
                    getMayGrantPermissionsNames: function () {

                        var mayGrantPermissions = AuthenticationServices.getMayGrantPermissions();

                        var mayGrantPermissionsNames = _.map(mayGrantPermissions, function (perm) {
                            return perm.name;
                        });

                        return mayGrantPermissionsNames;
                    },

                    // User permissions
                    getUserPermissionName: function () {
                        if (!_.isEmpty(AuthenticationServices.userProfile)) {
                            return AuthenticationServices.userProfile.roles[0].name;
                        } else {
                            return undefined;
                        }
                    },

                    // Check on config if user can create another user or edit one
                    userCanCreateOrEdit: function (pMayGrantNames) {

                        var mayGrantPermissionsNames = pMayGrantNames || AuthenticationServices.getMayGrantPermissionsNames();

                        // Check if user can create new users
                        var userCanCreate = _.intersection(mayGrantPermissionsNames, $rootScope.gfConfig.PermAvailableToCreate);

                        if (userCanCreate.length == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    },

                    canCreateBranches: function (userPermission) {

                        userPermission = userPermission || AuthenticationServices.getUserPermissionName();

                        var displayBranches = _.find($rootScope.gfConfig.PermAvailableToCreateBranches, function (br) {
                            return br == userPermission;
                        });

                        if (displayBranches) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    userCanSearch: function (userPermission) {
                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToSearch, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    // Check on config if user is available to search on any branch
                    userCanSearchOnAnyBranch: function (userPermission) {

                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToSearchOnAllBranches, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    // Check if user can create branches
                    userCanCreateBranches: function (userPermission) {

                        userPermission = userPermission || AuthenticationServices.getUserPermissionName();

                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToCreateBranches, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }

                    },

                    // Check if user can create sub-branches
                    userCanCreateSubBranches: function (userPermission) {
                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToCreateSubBranches, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    // Check if user can create branches (same as branches for now)
                    userCanCreateHouses: function (userPermission) {
                        return AuthenticationServices.userCanCreateBranches(userPermission);
                    },

                    getUserCustomer: function () {
                        return AuthenticationServices.userProfile.customer;
                    },

                    getUserSingleBranch: function () {
                        return AuthenticationServices.userProfile.branches[0];
                    },

                    getUserBranches: function () {
                        return AuthenticationServices.userProfile.branches;
                    },

                    // User is super admin
                    userIsSuperAdmin: function (permission) {
                        if (permission) {
                            return permission == $rootScope.gfConfig.Permissions.SuperAdmin;
                        }

                        if (AuthenticationServices.userIsLoggedIn()) {
                            return AuthenticationServices.userProfile.roles[0].name === $rootScope.gfConfig.Permissions.SuperAdmin;
                        }

                        return false;
                    },

                    userIsStudent: function () {
                        if (AuthenticationServices.userIsLoggedIn()) {
                            return AuthenticationServices.userProfile.roles[0].name === $rootScope.gfConfig.StudentRoleName;
                        }

                        return false;
                    },

                    userIsTeacher: function () {
                        if (AuthenticationServices.userIsLoggedIn()) {
                            return AuthenticationServices.userProfile.roles[0].name === $rootScope.gfConfig.TeacherRoleName;
                        }

                        return false;
                    },

                    userIsCustomerAdmin: function () {
                        if (AuthenticationServices.userIsLoggedIn()) {
                            return AuthenticationServices.userProfile.roles[0].name === $rootScope.gfConfig.Permissions.CustomerAdmin;
                        }

                        return false;
                    },

                    userIsBranchAdmin: function () {
                        if (AuthenticationServices.userIsLoggedIn()) {
                            return AuthenticationServices.userProfile.roles[0].name === $rootScope.gfConfig.Permissions.BranchAdmin;
                        }

                        return false;
                    },

                    userCanCreateGroups: function (userPermission) {

                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToCreateGroups, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }

                    },

                    userCanWatchLiveFeed: function (userPermission) {

                        var hasPerm = _.find($rootScope.gfConfig.PermAvailableToWatchLiveFeed, function (perm) {
                            return perm == userPermission;
                        });

                        if (hasPerm) {
                            return true;
                        } else {
                            return false;
                        }
                    },

                    getCharGender: function () {
                        return AuthenticationServices.userProfile.gender;
                    },

                    getAvatarURL: function () {
                        return AuthenticationServices.userProfile.avatarURL;
                    }



                }

                return AuthenticationServices;
            }]);

})();