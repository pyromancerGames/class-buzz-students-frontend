'use strict';
(function () {
    angular.module('griffinStudents.authentication')

        .factory('signupServices', ['$rootScope', '$state', 'apiServices', function ($rootScope, $state, apiServices) {

            var Services = {

                getBranchByRegCode: function (regCode) {
                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/branches/registerCode/' + regCode;


                    var req = {
                        method: 'GET',
                        url: _baseURL + _apiRoute,
                        headers: {},
                    };

                    return apiServices.apiRequest(req);
                }
            }

            return Services;
        }]);

})();