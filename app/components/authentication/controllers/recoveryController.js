(function() {
    'use strict';

    angular.module('griffinStudents.authentication')
        .controller('RecoveryCtrl', ['$rootScope', 'apiServices', '$state', 'socketioServices', 'authenticationServices', 'errorHandler', 'signupServices',
            function ($rootScope, apiServices, $state, socketioServices, authenticationServices, errorHandler, signupServices) {

                var viewModel = this;

                viewModel.recover = recoverPassword;
                viewModel.userEmail = "";
                viewModel.errorMsg = "";
                viewModel.infoMsg = "";



                function recoverPassword () {

                    viewModel.erroMsg = "";
                    viewModel.infoMsg = "";

                    var req = {
                        method: 'POST',
                        url: $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion + '/credentials/recovery',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {email:viewModel.userEmail}
                    };

                    apiServices.apiRequest(req)
                        .then(function(result){

                            if (result.data.status == "Error") {
                                viewModel.errorMsg = result.data.message;
                            }
                            else {
                                viewModel.userEmail = "";
                                viewModel.infoMsg = result.data.message;
                            }

                        })
                        .catch(function(err){
                            errorHandler.handleError(err, "Recovering Email");
                        })
                }

            }])
})();
