(function() {
    'use strict';

    angular.module('griffinStudents.authentication')
    .controller('SignupCtrl', ['$rootScope', 'apiServices', '$state', 'socketioServices', 'authenticationServices', 'errorHandler', 'signupServices',
    function ($rootScope, apiServices, $state, socketioServices, authenticationServices, errorHandler, signupServices) {

        var viewModel = this;
        viewModel.register = register;
        viewModel.checkUsername = checkUsername;
        viewModel.validateCI = validateCI;
        viewModel.onlyValidateCI = onlyValidateCI;
        viewModel.validateSecretCode = validateSecretCode;
        viewModel.userData = {};
        viewModel.secretCodeBranch = {};
        viewModel.validSecretCode = false;
        viewModel.userData.credentials = {};
        viewModel.userData.student = {};
        viewModel.userData.student.parent = {};
        viewModel.userData.student.parent2 = {};
        viewModel.userMsg = '';
        viewModel.registerCode = '';
        viewModel.usernameIsAvailable = false;
        viewModel.usernameIsValid = false;
        viewModel.validCI = false;
        viewModel.validParentCI = false;


        viewModel.registerSteps = [
            {
                templateUrl: '/app/components/authentication/views/registerWizard/step0.html',
                hasForm: true,
                title: 'Codigo Secreto'
            },

            {
                templateUrl: '/app/components/authentication/views/registerWizard/step1.html',
                hasForm: true,
                title: 'Ingresa tus credenciales'
            },

            {
                templateUrl: '/app/components/authentication/views/registerWizard/step2.html',
                hasForm: true,
                title: 'Ingresa tus datos personales'
            },

            {
                templateUrl: '/app/components/authentication/views/registerWizard/step3.html',
                hasForm: true,
                title: 'Los datos de uno de tus padres'
            },

            {
                templateUrl: '/app/components/authentication/views/registerWizard/step4.html',
                hasForm: true,
                title: 'Los datos del otro'
            }
        ];


        function usernameIsValid(username) {
            viewModel.usernameIsValid =  ( /^[0-9a-zA-Z_.-]+$/.test(username) && username.length >= 4);
            return viewModel.usernameIsValid;
        }


        function checkUsername (username){

            if (!usernameIsValid(username)) return;

            var req = {
                method: 'GET',
                url: $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion + '/usernameCheck/' + username,
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            apiServices.apiRequest(req)
                .then(function(result){
                    viewModel.usernameIsAvailable = result.data.available;
                })

        }

        function validateCI(ci){
            if (ci == "") return true;
            viewModel.validCI = ciValidator(ci);
            return viewModel.validCI;
        }

        function onlyValidateCI(ci){
            return ciValidator(ci);
        }

        function ciValidator(ci){

            if (!ci) return false;
            if (ci.length < 7) return false;

            var arrCoefs = [2,9,8,7,6,3,4,1];
            var suma = 0;
            var difCoef = parseInt(arrCoefs.length - ci.length);

            for (var i = ci.length - 1; i > -1; i--) {
                var dig = ci.substring(i, i+1);
                var digInt = parseInt(dig);
                var coef = arrCoefs[i+difCoef];
                suma = suma + digInt * coef;
            }

            if ( (suma % 10) == 0 ) {
                return true;
            } else {
                return false;

            }
        }

        function validateSecretCode () {
            viewModel.validSecretCode = false;
            signupServices.getBranchByRegCode(viewModel.registerCode)
                .then(function(res){
                    if (res.data) {
                        viewModel.secretCodeBranch = res.data;
                        viewModel.validSecretCode = true;
                    }
                })
        }


        function register(){
            viewModel.userMsg = '';

            if (!viewModel.validSecretCode) {
                viewModel.userMsg = "El codigo secreto no es valido";
                return;
            }

            if (!viewModel.usernameIsAvailable) {
                viewModel.userMsg = "El nombre de usuario seleccionado no esta disponible.";
                return;
            }

            checkForExistingUser()
                .then(processExistingUser)
                .then(addUserToDB)
                .then(insertSuccess)
                .catch(insertError)
        }

        function checkForExistingUser(){
            // Register code - get customer and branch from code
            var duplicateDocReq = {
                method: 'GET',
                url: $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion + '/users/exist/' + viewModel.userData.doc,
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            return apiServices.apiRequest(duplicateDocReq);
        }

        function processExistingUser(existingUser) {
            if (existingUser.data.exists) {
                console.log("dup CI");
                var e = new errorHandler.genericCustomError('Duplicate Key Error: $doc: ' + viewModel.userData.doc, "Lo sentimos, la cedula ingresada ya esta registrada.");
                throw e;
                return;
            }
            else {
                return fetchBranchByRegCode();
            }
        }


        function fetchBranchByRegCode() {
            // Register code - get customer and branch from code
            var codeReq = {
                method: 'GET',
                url: $rootScope.gfConfig.APIEndpoint + '/api/v1/branches/registerCode/' + viewModel.registerCode,
                headers: {
                    'Content-Type': 'application/json'
                }
            };

            return apiServices.apiRequest(codeReq);
        }


        function addUserToDB(branch){
            viewModel.userData.branches = [branch.data._id];
            viewModel.userData.customer = branch.data.customer;

            var req = {
                method: 'POST',
                url: $rootScope.gfConfig.APIEndpoint + '/api/v1/students/create',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: viewModel.userData
            };

            return apiServices.apiRequest(req)
        }

        function insertSuccess(res) {
            viewModel.userMsg = 'El estudiante fue creado exitosamente';
            doLogin();
        }

        function insertError(err){
            if (typeof err.userMessage != "undefined") viewModel.userMsg = err.userMessage;
            else viewModel.userMsg = 'Error al crear el estudiante, por favor intente mas tarde';
            errorHandler.handleError(err, 'Error while trying to create student');
        }


        function doLogin() {

            viewModel.erroMsg = '';

            var req = {
                method: 'POST',
                url: $rootScope.gfConfig.APIEndpoint + '/api/v1/authenticate',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {'username': viewModel.userData.credentials.username, 'password': viewModel.userData.credentials.password}
            }

            apiServices.apiRequest(req)

                .then(function (response) {

                    if (!response.data.token) {
                        throw new errorHandler.notAuthorizedError('Error while trying to login: Token not available on login response');
                        return;
                    }

                    // persist user
                    authenticationServices.setToken(response.data.token);
                    authenticationServices.setProfile(response.data.token);
                    socketioServices.initSocketIOEvents();

                    location.reload();
                    $state.go('main.home');
                    return;

                })
                .catch(function (err) {
                    viewModel.erroMsg = err.data.reason;
                });

        };

    }])


})();