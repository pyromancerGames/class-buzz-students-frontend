(function() {
    //'use strict';
    
    /**
    * @ngdoc function
    * @name griffinApp.authentication
    * @description
    * # Griffin authentication controller
    */
    angular.module('griffinStudents.authentication')
    .controller('AuthCtrl', ['$rootScope', 'apiServices', '$state', 'socketioServices', 'authenticationServices', 'errorHandler',
    function ($rootScope, apiServices, $state, socketioServices, authenticationServices, errorHandler) {


        var viewModel = this;

        viewModel.user = {};
        viewModel.erroMsg = '';
        viewModel.login = doLogin;
        viewModel.logout = authenticationServices.logout;
        viewModel.submitted = false;

        if (authenticationServices.userIsLoggedIn()) {
			if($state.current.name == 'main.login'){
            	$state.go('main.home');
			}
            return;
        }

        function doLogin() {

            viewModel.erroMsg = '';

            var req = {
                method: 'POST',
                url: $rootScope.gfConfig.APIEndpoint + '/api/v1/authenticate',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {'username': viewModel.user.userName, 'password': viewModel.user.pass}
            }

            apiServices.apiRequest(req)

                .then(function (response) {

                    if (!response.data.token) {
                        throw new errorHandler.notAuthorizedError('Error while trying to login: Token not available on login response');
                        return;
                    }

                    var _isStudent = false;
                    var _roles = response.data.user.roles;
                    _roles.forEach(function(r){
                        if (r.name == 'student') _isStudent = true;
                    })

                    if (_isStudent) {
                        // persist user
                        authenticationServices.setToken(response.data.token);
                        authenticationServices.setProfile(response.data.token);
                        socketioServices.initSocketIOEvents();

                        location.reload();
                        $state.go('main.home');
                        return;
                    }
                    else {
                        viewModel.erroMsg = "Debe ser estudiante para ingresar.";
                    }

                })
                .catch(function (err) {
                    viewModel.erroMsg = err.data.reason;
                });

            };

    }])

        .controller('ImpersonateCtrl', ['$rootScope', 'apiServices', '$state', 'socketioServices', 'authenticationServices', 'errorHandler',
            function ($rootScope, apiServices, $state, socketioServices, authenticationServices, errorHandler) {


                var viewModel = this;

                viewModel.user = {};
                viewModel.erroMsg = '';
                viewModel.login = doLogin;
                viewModel.logout = authenticationServices.logout;
                viewModel.submitted = false;
                viewModel.impersonationDoc = '';

                if (authenticationServices.userIsLoggedIn()) {
                    if($state.current.name == 'main.login'){
                        $state.go('main.home');
                    }
                    return;
                }

                function doLogin() {

                    viewModel.erroMsg = '';

                    var req = {
                        method: 'POST',
                        url: $rootScope.gfConfig.APIEndpoint + '/api/v1/impersonate',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            'username': viewModel.user.userName,
                            'password': viewModel.user.pass,
                            'impersonationDoc': viewModel.impersonationDoc}
                    }

                    apiServices.apiRequest(req)

                        .then(function (response) {

                            if (!response.data.token) {
                                throw new errorHandler.notAuthorizedError('Error while trying to login: Token not available on login response');
                                return;
                            }

                            // persist user
                            authenticationServices.setToken(response.data.token);
                            authenticationServices.setProfile(response.data.token);
                            socketioServices.initSocketIOEvents();

                            location.reload();
                            $state.go('main.home');
                            return;

                        })
                        .catch(function (err) {
                            viewModel.erroMsg = err.data.reason;
                        });

                };

            }])


    //.controller('ProfileCtrl', ['$rootScope', 'apiServices', '$state', 'authenticationServices', 'userServices', 'errorHandler',
    //function ($rootScope, apiServices, $state, authenticationServices, userServices, errorHandler) {
    //
    //    var viewModel = this;
    //
    //    // user avatar
    //    viewModel.getUserAvatarOrDefault = function() {
    //        return userServices.getUserAvatarOrDefault(authenticationServices.userProfile);
    //    }
    //}])


    .controller('PermCtrl', ['$rootScope', '$state', 'authenticationServices', 'errorHandler',
    function ($rootScope, $state, authenticationServices, errorHandler) {
        
        var viewModel = this;

        viewModel.userProfile = authenticationServices.getProfile;
        viewModel.userIsLoggedIn = authenticationServices.userIsLoggedIn;
        viewModel.userIsSuperAdmin = authenticationServices.userIsSuperAdmin;

        viewModel.userCanCreateBranches = authenticationServices.userCanCreateBranches;
        viewModel.userCanCreateHouses = authenticationServices.userCanCreateBranches;
        
    }])

})();