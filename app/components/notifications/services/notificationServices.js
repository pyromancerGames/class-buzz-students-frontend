(function () {

    angular.module('griffinStudents.notifications')

        .factory('notificationServices', [ '$http', '$rootScope', 'apiServices', function ($http, $rootScope, apiServices) {

            var notificationServices = {

                pendingNotifications: 0,

                getPendingNotifications: function (userId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/users/' + userId + '/messages';

                    var req = {
                        method: 'GET',
                        url:  _baseURL  + _apiRoute,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    };

                    return apiServices.apiRequest(req);
                },


                deleteNotification: function (notificationId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/messages/remove';

                    var req = {
                        method: 'POST',
                        url:  _baseURL  + _apiRoute,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {_id: notificationId}
                    };

                    return apiServices.apiRequest(req);

                },

                markAsRead: function (notificationId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/messages/markAsRead';
                    
                    var req = {
                        method: 'POST',
                        url:  _baseURL  + _apiRoute,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {_id: notificationId}
                    };

                    return apiServices.apiRequest(req);

                }
            };

            return notificationServices;
        }]);

})();