angular.module('griffinStudents.notifications')

    .controller('NotificationCtrl', ['$rootScope', '_', '$timeout', 'ngAudio', 'notificationServices', 'authenticationServices', 'userServices', 'errorHandler',
        function ($rootScope, _, $timeout, ngAudio, notificationServices, authenticationServices, userServices, errorHandler) {

            var viewModel = this;
            viewModel.userProfile = authenticationServices.getProfile();
            viewModel.userNotifications = [];
            viewModel.pendingNotifications = 0;

            viewModel.sounds = {
                newNotification: ngAudio.load("/app/public/sounds/fx/newNotification.wav"),
                notificationRead: ngAudio.load("/app/public/sounds/fx/whooshIn.wav")
            }

            viewModel.removeNotification = function(notif){
                $timeout(readNotification(notif),2000);
            }


            populateUserNotifications();

            $rootScope.$on('newnotification', function(event, notif){
                viewModel.pendingNotifications += 1;
                viewModel.sounds.newNotification.play();
                viewModel.userNotifications.push(notif)
            });

            $rootScope.$on('notificationread', function(notif){
                notif.isRead = true;
                viewModel.pendingNotifications -= 1;
                viewModel.sounds.notificationRead.play();
            });

            function readNotification(notif){

                notificationServices.markAsRead(notif._id)
                    .then(function(result){
                        var indexOf = viewModel.userNotifications.indexOf(notif);
                        if (indexOf != -1) viewModel.userNotifications.splice(indexOf,1);
                        $rootScope.$broadcast('notificationread', notif);
                    })
                    .catch(function (err) {
                        errorHandler.handleError(err);
                    })
            }


            function populateUserNotifications() {

                userServices.getUserNotifications(viewModel.userProfile)
                    .then(function (notifications) {
                        //viewModel.userNotifications = notifications.data;

                        notifications.data.forEach( function(n){
                            if (!n.isRead) {
                                viewModel.userNotifications.push(n);
                                viewModel.pendingNotifications += 1;
                            }
                        })

                    })
                    .catch(function (err) {
                        errorHandler.handleError(err);
                    })
            }



        }]);
