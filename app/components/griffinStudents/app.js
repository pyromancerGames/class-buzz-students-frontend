'use strict';
(function () {

    angular.module('griffinStudents', [
        'ngRoute',
        'ngAudio',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngStorage',
        'angular-jwt',
        'btford.socket-io',
        'angular-svg-round-progress',
        'isteven-omni-bar',
        'angular.filter',
		'ui.bootstrap',
        'multiStepForm',
		'angular-confirm',
        'griffinStudents.config',
        'griffinStudents.errors',
        'griffinStudents.charCanvas',
        'griffinStudents.version',
        'griffinStudents.authentication',
        'griffinStudents.api',
		'griffinStudents.user',
        'griffinStudents.quests',
        'griffinStudents.store',
        'griffinStudents.liveFeed',
        'griffinStudents.notifications',
        'griffinStudents.achievements',
        'griffinStudents.utils',
        'griffinStudents.settings',
        'griffinStudents.chat',
        'griffinStudents.rankings',
        'griffinStudents.home',
        'ui.router',
        'angular-momentjs',
        'ui.bootstrap'
    ])

})();
