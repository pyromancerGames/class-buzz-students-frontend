angular.module('griffinStudents')

    .config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
        $stateProvider

            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'components/griffinStudents/views/common/content.html',
                access: {allowAnonymous:false}
            })

            .state('main.home', {
                url: '/home',
                templateUrl: 'components/home/views/home.html',
                access: {allowAnonymous:false}
            })

            .state('main.recover', {
                url: "/recover",
                templateUrl: "components/authentication/views/passwordRecovery.html",
                data: {pageTitle: 'Recover Password'},
                access: {allowAnonymous:true}
            })

            .state('main.login', {
                url: "/login",
                templateUrl: "components/authentication/views/authenticationView.html",
                data: {pageTitle: 'Authenticate'},
                access: {allowAnonymous:true}
            })

            .state('main.impersonate', {
                url: "/impersonate",
                templateUrl: "components/authentication/views/impersonationView.html",
                data: {pageTitle: 'Impersonation'},
                access: {allowAnonymous:true}
            })

            .state('main.register', {
                url: "/register",
                templateUrl: "components/authentication/views/studentSignupView.html",
                data: {pageTitle: 'Registrate'},
                access: {allowAnonymous:true}
            })

            .state('main.charCanvas', {
                url: "/charCanvas",
                templateUrl: "components/charCanvas/views/charCanvasView.html",
                data: {pageTitle: 'Main'},
                access: {allowAnonymous:false}
            })

            .state('main.quests', {
                url: "/quests",
                templateUrl: "components/quests/views/questsMain.html",
                data: {pageTitle: 'Quests'},
                access: {allowAnonymous:false}
            })

            .state('main.notifications', {
                url: "/notifications",
                templateUrl: "components/notifications/views/notificationsMain.html",
                data: {pageTitle: 'Notifications'},
                access: {allowAnonymous:false}
            })

            .state('main.achievements', {
                url: "/achievements",
                templateUrl: "components/achievements/views/achievementsMain.html",
                data: {pageTitle: 'Achievements'},
                access: {allowAnonymous:false}
            })

            .state('main.liveFeed', {
                url: "/liveFeed",
                templateUrl: "components/liveFeed/views/liveFeedMain.html",
                data: {pageTitle: 'Live Feed'},
                access: {allowAnonymous:false}
            })

            .state('main.store', {
                url: "/store",
                templateUrl: "components/store/views/storeMain.html",
                data: {pageTitle: 'Store'},
                access: {allowAnonymous:false}
            })

            .state('main.error', {
                url: "/error",
                templateUrl: "components/errors/views/404.html",
                data: {pageTitle: 'Error'},
                access: {allowAnonymous:false}
            })

            .state('main.settings', {
                url: "/settings",
                templateUrl: "components/settings/views/settingsMain.html",
                data: {pageTitle: 'Settings'},
                access: {allowAnonymous:false}
            })

            .state('main.profile', {
                url: "/profile",
                templateUrl: "components/user/views/profileView.html",
                data: {pageTitle: 'Profile'},
                access: {allowAnonymous:false}
            })

            .state('main.changePassword', {
                url: "/changePassword",
                templateUrl: "components/user/views/changePassView.html",
                data: {pageTitle: 'Change Password'},
                access: {allowAnonymous:false}
            })

            .state('main.houseRanking', {
                url: "/houseRanking",
                templateUrl: "components/rankings/views/houseRankingView.html",
                data: {pageTitle: 'House Ranking'},
                access: {allowAnonymous:false}
            })

        $urlRouterProvider.otherwise("/main/login");

    }])

