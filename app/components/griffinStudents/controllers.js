angular.module('griffinStudents')
    .controller('MainCtrl', ['$rootScope', '$state', '$socketio', 'socketioServices','authenticationServices',
        function ($rootScope, $state, $socketio, socketioServices, authenticationServices) {

            var viewModel = this;
            viewModel.userProfile = authenticationServices.getProfile();

            if (authenticationServices.userIsLoggedIn()) socketioServices.initSocketIOEvents();

            $rootScope.$on('notificationdeleted', function () {
                viewModel.pendingNotifications -= 1;
            });

            $rootScope.$on('resetnotificationcount', function () {
                viewModel.pendingNotifications = 0;
            });

            $rootScope.$on('notificationread', function () {
                viewModel.pendingNotifications -= 1;
            })

            $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
                if (toState.access != undefined && !toState.access.allowAnonymous && !authenticationServices.userIsLoggedIn()) {
                        $state.transitionTo("main.login");
                        event.preventDefault();

                }
            });
            
        }])

    .controller('MenuCtrl', ['$rootScope', '$socketio','authenticationServices',
        function ($rootScope, $socketio, authenticationServices) {

            var viewModel = this;
            viewModel.notificationPopUpIsVisible = false;
            viewModel.questPopUpIsVisible = false;
            viewModel.optionsPopUpIsVisible = false;
            viewModel.newQuestIsAvailable = false;
            viewModel.bottomMobileMenuIsVisible = false;
            viewModel.leftMobileMenuIsVisible = false;
            viewModel.hideQuestPullUp = hideQuestPullUp;

            viewModel.userProfile = authenticationServices.getProfile();
            viewModel.toggleBottomMobileMenu = toggleBottomMobileMenu;
            viewModel.toggleLeftMobileMenu = toggleLeftMobileMenu;
            viewModel.toggleNotificationsPopUp = toggleNotificationsPopUp;
            viewModel.toggleQuestPopUp = toggleQuestPopUp;
            viewModel.toggleOptionsPopUp = toggleOptionsPopUp;
            viewModel.hideMobileMenus = hideMobileMenus;
            viewModel.hidePopUps = hidePopUps;

            function toggleNotificationsPopUp(){
                viewModel.notificationPopUpIsVisible = !viewModel.notificationPopUpIsVisible;
                viewModel.questPopUpIsVisible = false;
                viewModel.optionsPopUpIsVisible = false;
            }

            function toggleQuestPopUp(){
                viewModel.questPopUpIsVisible = !viewModel.questPopUpIsVisible;
                viewModel.notificationPopUpIsVisible = false;
                viewModel.optionsPopUpIsVisible = false;

            }

            function toggleOptionsPopUp(){
                viewModel.optionsPopUpIsVisible = !viewModel.optionsPopUpIsVisible;
                viewModel.notificationPopUpIsVisible = false;
                viewModel.questPopUpIsVisible = false;
            }

            function hidePopUps(){
                viewModel.questPopUpIsVisible = false;
                viewModel.notificationPopUpIsVisible = false;
                viewModel.optionsPopUpIsVisible = false;
            }

            function hideQuestPullUp(){
                viewModel.newQuestIsAvailable = false;
            }

            function toggleBottomMobileMenu () {
                viewModel.bottomMobileMenuIsVisible = !viewModel.bottomMobileMenuIsVisible;
                viewModel.leftMobileMenuIsVisible = false;
                hidePopUps();
            }

            function toggleLeftMobileMenu () {
                viewModel.leftMobileMenuIsVisible = !viewModel.leftMobileMenuIsVisible;
                hidePopUps();
                viewModel.bottomMobileMenuIsVisible = false;
            }

            function hideMobileMenus(){
                viewModel.bottomMobileMenuIsVisible = false;
                viewModel.leftMobileMenuIsVisible = false;
            }

            $rootScope.$on('newquestavailable', function(event, newQuest){
                viewModel.newQuestIsAvailable = true;
            })

        }])

    .controller('TabCtrl', [ function () {

            var viewModel = this;
            viewModel.activeTab = 1;

            viewModel.isSet = function(index){
                return index == viewModel.activeTab;
            }

            viewModel.setTab = function(index) {
                viewModel.activeTab = index;
            }

        }]);



