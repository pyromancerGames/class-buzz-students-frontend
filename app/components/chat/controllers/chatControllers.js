angular.module('griffinStudents.chat')

    .controller('ChatCtrl', ['$rootScope', '$timeout', '$socketio', '_', 'authenticationServices', 'ngAudio',
        function ($rootScope, $timeout, $socketio, _, authenticationServices, ngAudio) {
            var viewModel = this;
            viewModel.chatField = "";
            viewModel.chatHistory = [];
            viewModel.sendChatMessage = sendChatMessage;
            viewModel.chatViewisVisible = false;
            viewModel.unreadMessages = 0;
            viewModel.sounds = {
                newMessage: ngAudio.load("/app/public/sounds/fx/newChat.wav")
            }

            $rootScope.$on('newchatmessage', function(event, message){

                if (!viewModel.chatViewisVisible) {

                    viewModel.unreadMessages += 1;
                    viewModel.sounds.newMessage.play();
                }

                var chatMessage = JSON.parse(message);
                chatMessage.class = chatMessage.sender.id == authenticationServices.getProfile().id ? 'me' : 'you';
                viewModel.chatHistory.push(chatMessage);

                // Wait for DOM to update and then scroll to bottom!
                $timeout(function($scope){
                    var scrollbox = document.getElementsByClassName('chat-view');
                    scrollbox[0].scrollTop = scrollbox[0].scrollHeight + 100;
                },50);

            })

            viewModel.keyPressed = function(event){
                if (event.keyCode == 13) {
                    sendChatMessage();
                }
            }

            viewModel.toggleChatView = toggleChatView;

            function sendChatMessage (){
                if (_.isEmpty(viewModel.chatField)) return;

                var chatMsg = {
                    sender: authenticationServices.getProfile(),
                    message: viewModel.chatField
                }


                $socketio.emit('chatmessage', JSON.stringify(chatMsg));
                viewModel.chatField = "";
            }

            function toggleChatView (value){
                viewModel.unreadMessages = 0;
                viewModel.chatViewisVisible = value;
            }



        }]);
