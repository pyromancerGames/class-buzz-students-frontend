'use strict';
// TODO HOT: add animations on click (blick-say hi)

(function() {
	angular.module('griffinStudents.charCanvas')
	.directive('gameCanvas', function($injector) {
		
		var linkFn = function(scope, ele, attrs) {

			// Create game
			setTimeout(createCharCanvasGame, 500);
			
			function createCharCanvasGame() {
				
				// Chack modeltowatch init	
				if(!scope.modeltowatch) {
					// If not try to re init
					setTimeout(createCharCanvasGame, 500);
				} else {
					
					// Build the game object
					/*var height  = 400,
						width   = 400;
					var game = new Phaser.Game(
						width, 
						height, 
						Phaser.AUTO, 
						'gameCanvas', 
						{
							preload: preload, 
							create: create, 
							update: update
						},
						true
					);*/
					
					game.scope = scope;
					
					var body;
					var lowerBody;
					var upperBody;
					var head
					var eyes;
					var eyebrows;
					var mouth;
					
					/*function preload() {

						this.load.crossOrigin = "*";
						this.load.image('upperBody', this.game.scope.modeltowatch.charTrait.body.upperBody.foreImageURL);
						this.load.image('lowerBody', this.game.scope.modeltowatch.charTrait.body.lowerBody.foreImageURL);
						this.load.image('bodyhead', this.game.scope.modeltowatch.charTrait.body.head.foreImageURL);
						this.load.image('eyes', this.game.scope.modeltowatch.charTrait.body.eyes.foreImageURL);
						this.load.image('eyebrows', this.game.scope.modeltowatch.charTrait.body.eyebrows.foreImageURL);
						this.load.image('mouth', this.game.scope.modeltowatch.charTrait.body.mouth.foreImageURL);

					}

					function create() {

						var game = this.game;

						var params = {
							scale: {
								x: 1/4.5,
								y: 1/4.5
							}

						}

						body = game.add.sprite(game.world.centerX, game.world.centerY);
						body.scale.setTo(params.scale.x, params.scale.y);
						
						console.log('Body: ');
						console.log(body);
						//console.log(body.children[0].key);
						
						game.loadCaracterTraits();
					}*/
						
					/*function update() {
						
					}*/
					
					game.loadCaracterTraits = function () {
						
						//this.load.image('lowerBody', game.scope.modeltowatch.charTrait.body.upperBody.foreImageURL);
						
						
						//lowerBody.loadTexture('')
						
						
						lowerBody = body.addChild(game.make.sprite(-2,250,'lowerBody'));
						lowerBody.anchor.setTo(0.5, 0.5);

						upperBody = body.addChild(game.make.sprite(0, 0,'upperBody'));
						upperBody.anchor.setTo(0.5, 0.5);

						head = body.addChild(game.make.sprite(0,-430,'bodyhead'));
						head.anchor.setTo(0.5, 0.5);
						
						eyes = head.addChild(game.make.sprite(0, 110,'eyes'));
						eyes.anchor.setTo(0.5, 0.5);

						eyebrows = eyes.addChild(game.make.sprite(0, -130,'eyebrows'));
						eyebrows.anchor.setTo(0.5, 0.5);

						mouth = head.addChild(game.make.sprite(0, 300,'mouth'));
						mouth.anchor.setTo(0.5, 0.5);

						
					}
					
					game.loadCaracterTraits2 = function () {




						game.cache.removeImage("lowerBody");

						scope.modeltowatch.isLoading = true;
						
						var loadRes = this.load.image('lowerBody', scope.modeltowatch.charTrait.body.upperBody.foreImageURL, true).onLoadComplete.addOnce(function(res) {

						});
						
						this.load.onLoadComplete.addOnce(function(res) {

						});

					}
						
					scope.$watch(function () {
						return scope.modeltowatch;
					}, function(newValue, oldValue) {
						if(newValue) {
							if(newValue != oldValue) {

								if(newValue.updateModel) {
									game.loadCaracterTraits2();
									newValue.updateModel = false;
								}
							}
						}
					}, true);
				}
			}
		};
		
		return {
			restrict: 'E',
				scope: {
				players: '=',
				mapId: '=',
				modeltowatch: '='
			},
			template: '<div id="gameCanvas"></div>',
			link: linkFn
		}
		
	})
	.directive("charAvatar", function (authenticationServices ,userServices,charCanvasServices)
	{
		return {
			restrict: 'E',
			scope: {
				modeltowatch: '=',
				charname: '@',
				eyecolor: '@',
				hairModel: '@',
				hairColor: '@'
			},
			template: "<div id=\"canvasCharDiv\"></div>",
			link: function(scope, element, attrs) {
				
				scope.initialized = false;
				scope.blinkEnabled = true;
				scope.blinkImagePath = '/app/images/chars/blink01.png';
				scope.rev = 0;
				scope.isLoading = true;
				
				// Update FPS debug
				scope.updateFPS = function() {
					scope.curFPS = scope.numFramesDrawn;
					scope.numFramesDrawn = 0;
				}
				
				// Breath update
				scope.updateBreath = function() {
					if (scope.breathDir === 1) {  // breath in
						scope.breathAmt -= scope.breathInc;
						if (scope.breathAmt < -scope.breathMax) {
							scope.breathDir = -1;
						}
					} else {  // breath out
						scope.breathAmt += scope.breathInc;
						if(scope.breathAmt > scope.breathMax) {
							scope.breathDir = 1;
						}
					}
				}
				
				// Update blink
				scope.updateBlink = function () {
					scope.eyeOpenTime += scope.blinkUpdateTime;
					
					if(scope.eyeOpenTime >= scope.timeBtwBlinks){
						scope.blink();
					}
				}
				
				// Resources loaded
				scope.resourceLoaded = function () {
					// TODO HOT: get length of array in order to make the first draw
					scope.numResourcesLoaded += 1;
					if(scope.numResourcesLoaded === scope.totalResources) {
					
						setInterval(scope.redraw, 1000 / scope.fps);
					}
				}
				
				// Redraw char
				scope.redraw = function () {
				
					if(scope.initialized && !scope.isLoading) {
						scope.canvas.width = scope.canvas.width; // clears the canvas
						
						scope.drawEllipse(scope.canvasWidth / 2,  scope.canvasHeight - 30, 200 - (scope.breathAmt * 3), 15, 'black'); // Shadow
						
						// Back hair - Body
						if(scope.images["hairBack"]) {
							scope.context.drawImage(
								scope.images["hairBack"], 
								scope.canvasWidth / 2  - ((scope.images["hairBack"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.hair.offsetBackX, 
								20 / scope.assetsScale - scope.breathAmt + 80 + scope.modeltowatch.charTrait.body.hair.offsetBackY, 
								scope.images["hairBack"].width / scope.assetsScale, 
								scope.images["hairBack"].height / scope.assetsScale
							);
						}
						
						// Lowerbody - Body
						scope.context.drawImage(
							scope.images["lowerBody"], 
							scope.canvasWidth / 2  - ((scope.images["lowerBody"].width / scope.assetsScale) / 2), 
							scope.canvasHeight - scope.images["lowerBody"].height / scope.assetsScale - 30, 
							scope.images["lowerBody"].width / scope.assetsScale, 
							scope.images["lowerBody"].height / scope.assetsScale
						);
						
						// Upperbody - Body
						scope.context.drawImage(
							scope.images["upperBody"], 
							scope.canvasWidth / 2  - ((scope.images["upperBody"].width / scope.assetsScale) / 2),
							scope.canvasHeight - scope.images["upperBody"].height / scope.assetsScale - (scope.images["lowerBody"].height / scope.assetsScale) - scope.breathAmt - 10,
							//scope.canvasHeight - scope.images["upperBody"].height / scope.assetsScale - 100 - scope.breathAmt, 
							scope.images["upperBody"].width / scope.assetsScale, 
							scope.images["upperBody"].height / scope.assetsScale
						);
						
						// Feet - Wearable
						if(scope.images["feet"]) {
							scope.context.drawImage(
								scope.images["feet"], 
								scope.canvasWidth / 2  - ((scope.images["feet"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.feet.offsetX, 
								345 + scope.modeltowatch.charTrait.wearables.feet.offsetY,
								scope.images["feet"].width / scope.assetsScale, 
								scope.images["feet"].height / scope.assetsScale
							);
						}
						
						// Legs - Wearable
						if(scope.images["legs"]) {
							scope.context.drawImage(
								scope.images["legs"], 
								scope.canvasWidth / 2  - ((scope.images["legs"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.legs.offsetX, 
								316 + scope.modeltowatch.charTrait.wearables.legs.offsetY,
								scope.images["legs"].width / scope.assetsScale, 
								scope.images["legs"].height / scope.assetsScale
							);
						}
						// Chest - Wearable
						if(scope.images["chest"]) {
							scope.context.drawImage(
								scope.images["chest"], 
								scope.canvasWidth / 2  - ((scope.images["chest"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.chest.offsetX, 
								238 - scope.breathAmt + scope.modeltowatch.charTrait.wearables.chest.offsetY,
								scope.images["chest"].width / scope.assetsScale, 
								scope.images["chest"].height / scope.assetsScale
							);
						}
						
						// Head - Body
						scope.context.drawImage(
							scope.images["bodyhead"], 
							scope.canvasWidth / 2  - ((scope.images["bodyhead"].width / scope.assetsScale) / 2), 
							95 - scope.breathAmt,
							//scope.canvasHeight - (scope.images["bodyhead"].height / scope.assetsScale) - (scope.images["upperBody"].height / scope.assetsScale + scope.images["lowerBody"].height / scope.assetsScale) - scope.breathAmt + 12, 
							scope.images["bodyhead"].width / scope.assetsScale, 
							scope.images["bodyhead"].height / scope.assetsScale
						);
						
						// Eyebrows - Body
						if(scope.images["eyebrow"]) {
							scope.context.drawImage(scope.images["eyebrow"],
								scope.canvasWidth / 2  - ((scope.images["eyebrow"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.eyebrows.offsetX, 
								165 - scope.breathAmt + scope.modeltowatch.charTrait.body.eyebrows.offsetY, 
								scope.images["eyebrow"].width / scope.assetsScale, 
								scope.images["eyebrow"].height / scope.assetsScale
							);
						}
						
						// Eyes - Body
						if(scope.images["eyes"]) {
							if(!scope.blinking) {
								scope.context.drawImage(
									scope.images["eyes"], 
									scope.canvasWidth / 2  - ((scope.images["eyes"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.eyes.offsetX, 
									183 - scope.breathAmt + scope.modeltowatch.charTrait.body.eyes.offsetY, 
									scope.images["eyes"].width / scope.assetsScale, 
									scope.images["eyes"].height / scope.assetsScale
								);
							} else {
								scope.context.drawImage(
									scope.images["eyes"], 
									scope.canvasWidth / 2  - ((scope.images["eyes"].width / scope.assetsScale) / 2), 
									183 - scope.breathAmt, 
									scope.images["eyes"].width / scope.assetsScale, 
									scope.images["eyes"].height / scope.assetsScale
								);
							}
						}
						
						// Eyes - Wearable
						if(scope.images["wearableEyes"]) {
							scope.context.drawImage(
								scope.images["wearableEyes"], 
								scope.canvasWidth / 2  - ((scope.images["wearableEyes"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.eyes.offsetX, 
								174 - scope.breathAmt + scope.modeltowatch.charTrait.wearables.eyes.offsetY, 
								scope.images["wearableEyes"].width / scope.assetsScale, 
								scope.images["wearableEyes"].height / scope.assetsScale
							);
						}
						
						// Mouth - Body
						if(scope.images["mouth"]) {
							scope.context.drawImage(
								scope.images["mouth"], 
								scope.canvasWidth / 2  - ((scope.images["mouth"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.mouth.offsetX,
								235 - scope.breathAmt + scope.modeltowatch.charTrait.body.mouth.offsetY, 
								scope.images["mouth"].width / scope.assetsScale, 
								scope.images["mouth"].height / scope.assetsScale
							);
						}
						
						// Facial hair - Body
						if(scope.images["facialHair"]) {
							scope.context.drawImage(
								scope.images["facialHair"], 
								scope.canvasWidth / 2  - ((scope.images["facialHair"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.facialHair.offsetX,  
								220 - scope.breathAmt + scope.modeltowatch.charTrait.body.facialHair.offsetY,
								scope.images["facialHair"].width / scope.assetsScale, 
								scope.images["facialHair"].height / scope.assetsScale
							);
						}
						
						// Fore hair - Body
						if(scope.images["hairFore"]) {
							scope.context.drawImage(
								scope.images["hairFore"], 
								scope.canvasWidth / 2  - ((scope.images["hairFore"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.hair.offsetX, 
								20 / scope.assetsScale - scope.breathAmt + 80 + scope.modeltowatch.charTrait.body.hair.offsetY, 
								scope.images["hairFore"].width / scope.assetsScale, 
								scope.images["hairFore"].height / scope.assetsScale
							);
						}
						
						// Righ Hand - Wearable
						if(scope.images["rightHand"]) {
							scope.context.drawImage(
								scope.images["rightHand"], 
								108 + scope.canvasWidth / 2  - ((scope.images["rightHand"].width / scope.assetsScale) / 2) + + scope.modeltowatch.charTrait.wearables.rightHand.offsetX, 
								193 - scope.breathAmt + scope.modeltowatch.charTrait.wearables.rightHand.offsetY, 
								scope.images["rightHand"].width / scope.assetsScale, 
								scope.images["rightHand"].height / scope.assetsScale
							);
						}
						
						if(scope.images["hat"]) {
							scope.context.drawImage(
								scope.images["hat"], 
								scope.canvasWidth / 2  - ((scope.images["hat"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.head.offsetX, 
								20 / scope.assetsScale - scope.breathAmt + 68 + scope.modeltowatch.charTrait.wearables.head.offsetY, 
								scope.images["hat"].width / scope.assetsScale, 
								scope.images["hat"].height / scope.assetsScale
							);
						}


						// Companion Right
						if(scope.images["companionRight"]) {
							scope.context.drawImage(
									scope.images["companionRight"],
									-150 + scope.canvasWidth / 2  - ((scope.images["companionRight"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.wearables.companionRight.offsetX,
									20 / scope.assetsScale + 242 + scope.modeltowatch.charTrait.wearables.companionRight.offsetY,
									scope.images["companionRight"].width / (scope.assetsScale - 2),
									scope.images["companionRight"].height / (scope.assetsScale - 2)
							);
						}
					}
				}
				
				// Hex to Rgb converter
				scope.hexToRgb = function (color) {
					var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
					color = color.replace(shorthandRegex, function(m, r, g, b) {
						return r + r + g + g + b + b;
					});
					
					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
					return result ? {
						r: parseInt(result[1], 16),
						g: parseInt(result[2], 16),
						b: parseInt(result[3], 16)
					} : {
						r: 0,
						g: 0,
						b: 0
					};
				}
				
				// Draw ellipse
				scope.drawEllipse = function (centerX, centerY, width, height, fillStyleColor) {
				
					scope.context.beginPath();
					
					scope.context.moveTo(centerX, centerY - height/2);
					
					scope.context.bezierCurveTo(
						centerX + width/2, centerY - height/2,
						centerX + width/2, centerY + height/2,
						centerX, centerY + height/2
					);
					
					scope.context.bezierCurveTo(
						centerX - width/2, centerY + height/2,
						centerX - width/2, centerY - height/2,
						centerX, centerY - height/2
					);
					
					scope.context.fillStyle = fillStyleColor;
					scope.context.fill();
					scope.context.closePath();	
				}
				
				// Char blink
				scope.blink = function () {
					if(scope.initialized) {
						if(scope.modeltowatch.generateAvatarURL) {
							scope.blinkEnabled = false;
							if(!scope.updatingAvatar) {

								scope.blinking = false;
								scope.eyeOpenTime = 0;
								scope.curEyeHeight = scope.maxEyeHeight;
								scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.eyes.placement, scope.initialEyes, true);
								scope.context.drawImage(
										scope.images["eyes"],
										scope.canvasWidth / 2  - ((scope.images["eyes"].width / scope.assetsScale) / 2) + scope.modeltowatch.charTrait.body.eyes.offsetX,
										183 - scope.breathAmt + scope.modeltowatch.charTrait.body.eyes.offsetY,
										scope.images["eyes"].width / scope.assetsScale,
										scope.images["eyes"].height / scope.assetsScale);
								setTimeout(setAvatarURL, 1500);
							}
						} else {
							if(scope.blinkEnabled) {
								scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.eyes.placement, scope.blinkImagePath, true);
								scope.curEyeHeight -= 2;
								if (scope.curEyeHeight <= 0) {
									scope.blinking = false;
									scope.eyeOpenTime = 0;
									scope.curEyeHeight = scope.maxEyeHeight;
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.eyes.placement, scope.initialEyes, true);
								} else {
									scope.blinking = true;
									setTimeout(scope.blink, 10);
								}
							} else {
								setTimeout(scope.blink, 10);
								scope.blinking = false;
							}
						}
					} else {
						setTimeout(scope.blink, 10);
					}
				}
				
				function setAvatarURL() {
					scope.updatingAvatar = true;

					userServices.updateUserAvatar(scope.canvas.toDataURL())
					.then(function(res) {

						scope.updatingAvatar = false;
						scope.modeltowatch.generateAvatarURL = false;
					})
					.catch(function(err) {
						console.log(err);
					})
				}
			
				scope.images = {};
				scope.totalResources = 3;
				scope.numResourcesLoaded = 0;
				scope.fps = 30;
				scope.x = 200;
				scope.y = 450;
				scope.breathInc = 0.1;
				scope.breathDir = 1;
				scope.breathAmt = 0;
				scope.breathMax = 1.5;
				scope.breathInterval = setInterval(scope.updateBreath, 1000 / scope.fps);
				scope.maxEyeHeight = 14;
				scope.curEyeHeight = scope.maxEyeHeight;
				scope.eyeOpenTime = 0;
				scope.timeBtwBlinks = 4000;
				scope.blinkUpdateTime = 200;                    
				scope.blinkTimer = setInterval(scope.updateBlink, scope.blinkUpdateTime);
				scope.fpsInterval = setInterval(scope.updateFPS, 1000);
				scope.numFramesDrawn = 0;
				scope.curFPS = 0;
				scope.initialEyes = '';
				scope.assetsScale = 4.5;
				scope.canvasWidth  = 400;
  				scope.canvasHeight = 400;
				scope.blinking = false;
				scope.updatingAvatar = false;
			
				// Init
				// Create the canvas (Neccessary for IE because it doesn't know what a canvas element is)
				scope.canvas = document.createElement('canvas');
				scope.canvas.setAttribute('width', scope.canvasWidth);
				scope.canvas.setAttribute('height', scope.canvasHeight);
				scope.canvas.setAttribute('id', 'canvas');
				scope.canvasDiv = document.getElementById("canvasCharDiv");
				scope.canvasDiv.appendChild(scope.canvas);
				
				if(typeof G_vmlCanvasManager != 'undefined') {
					scope.canvas = G_vmlCanvasManager.initElement(scope.canvas);
				}
			
				// Get 2d canvas context
				scope.context = scope.canvas.getContext('2d');
				// Note: The above code is a workaround for IE 8and lower. Otherwise we could have used:
				//     context = document.getElementById('canvas').getContext("2d");
				
				
				
				// Load char images
				scope.loadImage = function (name, model, imgColor) {
					
					scope.images[name] = new Image();
					scope.images[name].setAttribute('crossOrigin', 'anonymous');
					
					scope.images[name].onload = function() { 
						scope.resourceLoaded();
					}
					
					if(model) {
						// TODO HOT: read full path from aws
						scope.images[name].src = "images/chars/" + name + '/' + model + '-' + imgColor +".png";
					} else {
						scope.images[name].src = "images/" + name  +".png";
					}
				};
				
				// Load char images
				scope.loadAbsoluteImage = function (name, imgSrc, isBlink) {

					scope.images[name] = new Image();
					// Set img atts
					scope.images[name].setAttribute('crossOrigin', '*');
					scope.images[name].onload = function() {
						scope.resourceLoaded();
					};
					scope.images[name].src = imgSrc + '?rev=' + scope.rev;
					return;

				};
				
				scope.$watch(function () {
					return scope.modeltowatch;
				}, function(newValue, oldValue) {
					if(newValue) {
						if(newValue != oldValue) {

							if(newValue.updateModel) {
								loadTrait();
								newValue.updateModel = false;
							}
						}
					} else {
						//console.log('Not value on sopeWatch, skipping');
					}
				}, true);
				
				
				// Load char
				function loadTrait() {
					
					if(scope.modeltowatch.charTrait) {
						scope.isLoading = true;
						charCanvasServices.getTraitsVersion()
								.then(function(res) {

									scope.rev = res.data.version;
									// Body

									// Head - body
									scope.loadAbsoluteImage('bodyhead', scope.modeltowatch.charTrait.body.head.foreImageURL);
									// Upperbody
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.upperBody.placement, scope.modeltowatch.charTrait.body.upperBody.foreImageURL);
									// Lowerboddy
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.lowerBody.placement, scope.modeltowatch.charTrait.body.lowerBody.foreImageURL);
									// Hair back
									if(scope.modeltowatch.charTrait.body.hair && scope.modeltowatch.charTrait.body.hair.backImageURL) {
										scope.loadAbsoluteImage('hairBack', scope.modeltowatch.charTrait.body.hair.backImageURL);
									} else {
										scope.images["hairBack"] = null;
									}
									// Hair fore
									if(scope.modeltowatch.charTrait.body.hair && scope.modeltowatch.charTrait.body.hair.foreImageURL) {
										scope.loadAbsoluteImage('hairFore', scope.modeltowatch.charTrait.body.hair.foreImageURL);
									} else {
										scope.images["hairFore"] = null;
									}
									// Eyes
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.eyes.placement, scope.modeltowatch.charTrait.body.eyes.foreImageURL);
									// Eyebrows
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.eyebrows.placement, scope.modeltowatch.charTrait.body.eyebrows.foreImageURL);
									// Mouth
									scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.mouth.placement, scope.modeltowatch.charTrait.body.mouth.foreImageURL);

									// Facial Hair
									if(scope.modeltowatch.charTrait.body.facialHair) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.body.facialHair.placement, scope.modeltowatch.charTrait.body.facialHair.foreImageURL);
									} else {
										scope.images["facialHair"] = null;
									}


									// Wearables / Misc

									// Chest
									if(scope.modeltowatch.charTrait.wearables.chest) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.wearables.chest.placement, scope.modeltowatch.charTrait.wearables.chest.foreImageURL);
									} else {
										scope.images["chest"] = null;
									}

									// Legs
									if(scope.modeltowatch.charTrait.wearables.legs) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.wearables.legs.placement, scope.modeltowatch.charTrait.wearables.legs.foreImageURL);
									} else {
										scope.images["legs"] = null;
									}

									// Feet
									if(scope.modeltowatch.charTrait.wearables.feet) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.wearables.feet.placement, scope.modeltowatch.charTrait.wearables.feet.foreImageURL);
									} else {
										scope.images["feet"] = null;
									}

									// Head
									if(scope.modeltowatch.charTrait.wearables.head) {
										scope.loadAbsoluteImage('hat', scope.modeltowatch.charTrait.wearables.head.foreImageURL);
									} else {
										scope.images["hat"] = null;
									}

									// Eyes
									if(scope.modeltowatch.charTrait.wearables.eyes) {
										scope.loadAbsoluteImage('wearableEyes', scope.modeltowatch.charTrait.wearables.eyes.foreImageURL);
									} else {
										scope.images["wearableEyes"] = null;
									}

									// Right hand
									if(scope.modeltowatch.charTrait.wearables.rightHand) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.wearables.rightHand.placement, scope.modeltowatch.charTrait.wearables.rightHand.foreImageURL);
									} else {
										scope.images["rightHand"] = null;
									}

									// Companion right
									if(scope.modeltowatch.charTrait.wearables.companionRight) {
										scope.loadAbsoluteImage(scope.modeltowatch.charTrait.wearables.companionRight.placement, scope.modeltowatch.charTrait.wearables.companionRight.foreImageURL);
									} else {
										scope.images["companionRight"] = null;
									}

									scope.isLoading = false;

									// Save initial eyes for blink anim
									scope.initialEyes = scope.modeltowatch.charTrait.body.eyes.foreImageURL;

									scope.initialized = true;
								})
								.catch(function(err) {
									console.log(err)
								});
					}
				}
			}
		};
	});
})();

/*

"_id":"566ac306a7830e831f38b098",
"name":"Male 1",
"__v":0,
"wearables":
	{"rightHand":null,"leftHand":null,"feet":null,"eyes":null,"legs":null,"chest":null,"head":null},
"body":
	{"faceHair":null,
	
	"hair":{"_id":"566749582ca92f7e56ba5272","name":"Lady hair 1","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":1},"price":200,"backImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FPhl5R.png","foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2Fj3FwB.png","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FCD2Xz.png","placement":"hair","traitType":"body"}
	
	"mouth":{"_id":"5668849fe079d55c0bf23587","name":"Happy mouth","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":0},"price":0,"backImageURL":null,"foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FI3KZO.png","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FzJVzs.png","placement":"mouth","traitType":"body"},
	
	"eyebrows":{"_id":"566885fde079d55c0bf23588","name":"Big eyebrows - Black","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":5},"price":10,"backImageURL":null,"foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2Fx1aqA.png","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FrJX2p.png","placement":"eyebrow","traitType":"body"},
	
	"eyes":{"_id":"56682e971eb463b9063e4ea7","name":"Big eyes 1 black","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":0},"price":0,"backImageURL":null,"foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FjY955.png","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FuYLp8.png","placement":"eyes","traitType":"body"},
	
	"lowerBody":{"_id":"566995a362dd58d015dcee04","name":"Lower Body - Pink","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":0},"price":0,"backImageURL":null,"foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FEFSwUHknDCLcGIXcZNB4.jpg","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FLjgEpjL1xA7nHj6p4ReW.jpg","placement":"lowerBody","traitType":"body"},
	
	"upperBody":{"_id":"5669825eefb5da09149c936a","name":"Upper Body - Pink","__v":0,"visible":true,"enabled":true,"requirements":{"trait":null,"level":0},"price":0,"backImageURL":null,"foreImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FEmHcJ8ADiWXEkFHVkrsk.jpg","previewImageURL":"http://pyromancer.co.gg.images.s3.amazonaws.com/uploads%2FLmhbq3VuBn3oSn9zM9aU.jpg","placement":"upperBody","traitType":"body"}},
"gender":"M"}


*/
