'use strict';
(function() {
	angular.module('griffinStudents.charCanvas')
	.controller('CharCanvasCtrl',
	['$scope', '$uibModal', 'authenticationServices', 'charCanvasServices', 'userServices', '$timeout' ,function
	($scope, $uibModal,authenticationServices, charCanvasServices, userServices, $timeout) {

		var viewModel = this;

		var firstTimeLoaded;
		viewModel.trait = {};
		viewModel.user = {};
		viewModel.allTraits = [];
		viewModel.character = {};

		// Set default selected values
		viewModel.mSelected = 'body';
		viewModel.pSelected = 'eyes';

		viewModel.updateModelSelected = updateModelSelected;
		viewModel.updatePartSelected = updatePartSelected;
		viewModel.setPreviewPart = setPreviewPart;
		viewModel.updateCharEquipedTraits = updateCharEquipedTraits;
		viewModel.clearCurrentPart = clearCurrentPart;
		viewModel.cancelUpdate = cancelUpdate;
		viewModel.checkActive = checkActive;
		viewModel.buyTrait = buyTrait;
		viewModel.setPreviewPartForNotAvilable = setPreviewPartForNotAvilable;
		viewModel.isLoading = true;
		viewModel.equipEnabled = false;
		viewModel.displayClearButton = false;
		viewModel.dirtyTrait = false;
		viewModel.checkEquipedTraits = checkEquipedTraits;
		viewModel.checkSelectedBodyColour = checkSelectedBodyColour;
		
		/*Pagination*/
        viewModel.page = 0;
        viewModel.totalPages = 0;
        viewModel.itemsPerPage = 12;
        viewModel.usersAutocompleteData = [];
        viewModel.prevPageDisabled = prevPageDisabled;
        viewModel.prevPage = prevPage;
        viewModel.setPage = setPage;
        viewModel.nextPage = nextPage;
        viewModel.nextPageDisabled = nextPageDisabled;
        viewModel.pageCount = pageCount;
		viewModel.navigateEnabled = true;
        viewModel.paginationControllers = [];

		function pageCount() {
            return Math.ceil(viewModel.totalItems/viewModel.itemsPerPage);
        }

        function prevPageDisabled() {
			if(!viewModel.navigateEnabled || viewModel.page < 2) {
				return  "disabled";
			} else {
				return "";
			}
        }

        function prevPage() {
			if(viewModel.navigateEnabled) {
				if (viewModel.page > 1) {
					viewModel.page--;
					disableNavigation();
				}
			}
        }

        function setPage(n) {
			if(viewModel.navigateEnabled) {
				if (n > 0 && n < pageCount() + 1) {
					viewModel.page = n;
					disableNavigation();
				}

			}
        }

        function nextPageDisabled() {
			if(!viewModel.navigateEnabled || viewModel.page === pageCount()) {
				return "disabled";
			} else {
				return "";
			}
        }

        function nextPage() {
			if(viewModel.navigateEnabled) {
				if (viewModel.page < pageCount()) {
					viewModel.page++;
					disableNavigation();
				}

			}
        }

		function disableNavigation() {
			viewModel.navigateEnabled = false;
			$timeout(function() {
				viewModel.navigateEnabled = true;
				return;
			}, 1000);
		}

		function functionPaginationControllers() {

            var rangeSize = 5;

            var start;

            start = viewModel.page;

            if(viewModel.pageCount() < rangeSize) {
                rangeSize = viewModel.pageCount();
            }

            if ( start > viewModel.pageCount() - rangeSize ) {
                start = viewModel.pageCount() - rangeSize;
            }

            var newPagination = [];

            for (var i = start; i < start + rangeSize; i++) {
                newPagination.push(i);
            }

			if (newPagination.indexOf(viewModel.page - 1) == -1 ){
				newPagination.unshift(viewModel.page - 1);
			}

            viewModel.paginationControllers = newPagination;

        }
		
		// Change skin tone
		 function checkSelectedBodyColour(val) {
			
			viewModel.isLoading = true;
			
			var upperBody;
			var lowerBody;
			var	head;
			
			charCanvasServices.getSingleTrait('Body-Head-' + val)
			.then(function(resHead) {
				head = resHead.data;
				head.availableForUser = true;
				charCanvasServices.getSingleTrait('Body-Legs-' + val)
				.then(function(resLowerbody) {
					lowerBody = resLowerbody.data;
					lowerBody.availableForUser = true;
					charCanvasServices.getSingleTrait('Body-Chest-' + val)
					.then(function(resUp) {
						upperBody = resUp.data;
						upperBody.availableForUser = true;
						
						// Set trait color;
						viewModel.equipEnabled = true;
						
						viewModel.trait['body'][upperBody.placement] = upperBody;
						viewModel.trait['body'][lowerBody.placement] = lowerBody;
						viewModel.trait['body'][head.placement] = head;
						
						viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait};
						checkDirtyTraits();
						
						viewModel.isLoading = false;
					})
				})	
			})	
			.catch(function(err) {
				
			});
		}
		
		function showNotBuyableTraitModel() {
			$uibModal.open({
					animation: true,
					template: "<div class='cant-buy-traitcontainer'><p class='modal-info-text'>You can't buy this trait yet!</p><div class='modal-buttons'><button ng-click='closeNotBuyableTraitModal()'>OK</button></div></div>",
					backdrop: true,
					controller:function($uibModalInstance ,$scope){
					$scope.closeNotBuyableTraitModal = function () {
						$uibModalInstance.dismiss('cancel');
					};

    			},
			});
		}

		$scope.$watch(function () {
            return viewModel.page;
        }, function(newValue, oldValue) {
            if(newValue == 0) {
                getSelectedTraitsAvailable(1);
            } else {
                if(newValue != oldValue) {
                    getSelectedTraitsAvailable(newValue);
                }
            }
        });

		/* Pagination Ends*/

		// Get user
		userServices.getUser(authenticationServices.getUserDoc())
		.then(function(usrRes) {
			// Set user
			viewModel.user = usrRes.data;

			// Get character
			userServices.getUserCharacter(authenticationServices.getUserDoc())
			.then(function(charRes) {

				viewModel.character = charRes.data;
				firstTimeLoaded = viewModel.character.traits.body.upperBody == null;

				// Traits
				if(firstTimeLoaded) {

					var charGender = authenticationServices.getCharGender() || 'M';

					charCanvasServices.getDefaultTrait(charGender)
					.then(function(res) {
						// Equip
						viewModel.trait = res.data;
						setAvailableForUserDefaults();
						viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait, generateAvatarURL: false};
						viewModel.isLoading = false;
						getSelectedTraitsAvailable(1);
						updateCharEquipedTraits();
					})
				} else {
					// Equipped
					viewModel.trait = viewModel.character.traits;
					setAvailableForUserDefaults();
					viewModel.trait.body.eyebrow = viewModel.trait.body.eyebrows;
					viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait, generateAvatarURL: false};
					viewModel.isLoading = false;
					getSelectedTraitsAvailable(1);
				}
			})
		})
		.catch(function(err){
			console.log(err);
		});

		// Reset character values
		function cancelUpdate() {
			viewModel.isLoading = true;
			userServices.getUserCharacter(authenticationServices.getUserDoc())
			.then(function(res) {
				viewModel.character = res.data;
				viewModel.trait = viewModel.character.traits;
				setAvailableForUserDefaults();
				viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait};
				viewModel.isLoading = false;
				viewModel.equipEnabled = false;
			})
			.catch(function(err){
				console.log(err);
			});
		}

		function setAvailableForUserDefaults() {

			for (var key in viewModel.trait.body) {
				if(viewModel.trait.body[key]) {
					viewModel.trait.body[key].availableForUser = true;
				}
			}

			for (var key in viewModel.trait.wearables) {
				if(viewModel.trait.wearables[key]) {
					viewModel.trait.wearables[key].availableForUser = true;
				}
			}
		}

		// Check active part
		function checkActive(trait) {
			if(!viewModel.isLoading) {
				var partModel = viewModel.mSelected == 'wearable' ? 'wearables' : viewModel.mSelected;
				if(viewModel.trait[partModel] && viewModel.trait[partModel][trait.placement]) {
					if(viewModel.trait[partModel][trait.placement]['foreImageURL'] == trait.foreImageURL) {
						return true;
					}
				}
			}
			return false;
		}

		// Display or hide clear part button
		function checkClearDisplayButton() {

			var partModel = viewModel.mSelected == 'wearable' ? 'wearables' : viewModel.mSelected;

			if(partModel == 'body') {
				// Body
				if(viewModel.pSelected == 'hair' || viewModel.pSelected == 'facialHair') {
					viewModel.displayClearButton = true;
				} else {
					viewModel.displayClearButton = false;
				}

			} else {
				// Wearables
				if(viewModel.pSelected == 'chest' || viewModel.pSelected == 'legs') {
					viewModel.displayClearButton = false;
				} else {
					viewModel.displayClearButton = true;
				}
			}
		}

		// Update selected model
		function updateModelSelected(modelSelected) {

			viewModel.mSelected = modelSelected;

			// Set default trait
			viewModel.pSelected = 'eyes';
			getSelectedTraitsAvailable(1);
		}

		// Update selected part
		function updatePartSelected(partSelected) {
			viewModel.pSelected = partSelected;
			getSelectedTraitsAvailable(1);
		}

		// Clear selected part
		function clearCurrentPart() {

			var partModel = viewModel.mSelected == 'wearable' ? 'wearables' : viewModel.mSelected;

			if(viewModel.pSelected == 'eyebrow') {
				viewModel.trait[partModel]['eyebrows'] = null;
			} else {
				viewModel.trait[partModel][viewModel.pSelected] = null;
			}

			viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait};
			viewModel.isLoading = false;
			viewModel.equipEnabled = true;
			checkDirtyTraits();
		}

		function filterAvailableTraits() {

			if(!viewModel.isLoading && viewModel.character) {

				if(!viewModel.character.inventory) {
					viewModel.character.inventory = [];
				}

				viewModel.allTraits.forEach(function(element) {

					// Init default value as not available
					element.availableForUser = false;

					var i = viewModel.character.inventory.indexOf(element._id);

					// Exists inside charInventory or it's free
					if(i != -1 || element.price == 0 && element.requirements.level <= viewModel.character.levelInfo.level) {
						element.availableForUser = true;
						return;
					}

					// Available to buy
					element.userCanBuy = charCanBuyTrait(element);

				}, this);
			}
		}

		function getSelectedTraitsAvailable(forPage) {

			viewModel.traitsLoaded = false;

			var  filters = {
                        traitType: viewModel.mSelected,
                        placement: viewModel.pSelected
                    };

			charCanvasServices.getTraits('', filters, viewModel.itemsPerPage, forPage)
			.then( function(res) {
				viewModel.allTraits = res.data[0];
				viewModel.totalPages = res.data[1];
				viewModel.totalItems = res.data[2];
				functionPaginationControllers();
				viewModel.page = forPage;
				filterAvailableTraits();
				viewModel.traitsLoaded = true;
				checkClearDisplayButton();
			});
		}

		function buyTrait(trait) {

			// Get updated values
			userServices.getUserCharacter(authenticationServices.getUserDoc())
			.then(function(res) {

				viewModel.character = res.data;

				var canBuyTrait = charCanBuyTrait(trait);

				if(canBuyTrait) {


					if(!viewModel.character.inventory) {
						viewModel.character.inventory = [];
					}

					updateBoughtTraits(trait._id);

					// TODO HOT: display modal with new item bought
					getSelectedTraitsAvailable(1);

				} else {
					showNotBuyableTraitModel();
					console.log('User cannot buy this item');
				}

			})
			.catch(function(err) {
				console.log(err);
			})
		}

		function charCanBuyTrait(trait) {
			var canBuyTrait = true;

			if(!viewModel.isLoading) {

				if(viewModel.user.student){

					// Check user doublouns
					if(trait.price > viewModel.user.student.currency.doubloons) {
						canBuyTrait = false;
					}

					// Check level req
					if(canBuyTrait && trait.requirements.level > viewModel.character.levelInfo.level) {
						canBuyTrait = false;
					}

					// Check trait req
					if(canBuyTrait && trait.requirements.trait) {
						canBuyTrait = checkForRequieredTrait(trait);
					}
				}

				return canBuyTrait;

			} else {
				return false;
			}

		}

		function checkForRequieredTrait(trait) {

			var hasReq = false;

			viewModel.character.inventory.forEach(function(element) {

				var rq = trait._id == element;

				if(rq) {
					// Exists inside charInventory
					hasReq = true;
					return;
				}

			}, this);

			return hasReq;
		}

		function setPreviewPart(partModel, trait) {

			viewModel.isLoading = true;
			
			viewModel.equipEnabled = true;
			
			partModel = partModel == 'wearable' ? 'wearables' : partModel;
			
			if(trait.placement == 'eyebrow') {
				viewModel.trait[partModel]['eyebrows'] = trait;
			}
			
			viewModel.trait[partModel][trait.placement] = trait;
			viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait};
			checkDirtyTraits();
			viewModel.isLoading = false;
		}

		// Cheks that the user doesnt have a preview item equiped
		function checkDirtyTraits() {

			for (var key in viewModel.trait.body) {
				if(viewModel.trait.body[key]) {
					if(!viewModel.trait.body[key].availableForUser) {
						viewModel.dirtyTrait = true;
						return;
					}
				}
			}

			for (var key in viewModel.trait.wearables) {
				if(viewModel.trait.wearables[key]) {
					if(!viewModel.trait.wearables[key].availableForUser) {
						viewModel.dirtyTrait = true;
						return;
					}
				}
			}

			viewModel.dirtyTrait = false;
			return;
		}


		// Set preview of a not bought part
		function setPreviewPartForNotAvilable(partModel, trait) {

			if(!trait.availableForUser) {

				viewModel.isLoading = true;

				partModel = partModel == 'wearable' ? 'wearables' : partModel;

				if(trait.placement == 'eyebrow') {
					viewModel.trait[partModel]['eyebrows'] = trait;
				}

				viewModel.trait[partModel][trait.placement] = trait;
				viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait};
				checkDirtyTraits();
				viewModel.isLoading = false;
			} else {
				setPreviewPart(partModel, trait);
			}

		}

		function checkEquipedTraits() {
			return viewModel.equipEnabled && !viewModel.dirtyTrait;
		}

		$scope.$on('itembought', function(){
			updatePartSelected(viewModel.pSelected);
		});

		function updateBoughtTraits(traitId) {

			// Set new values
			console.log('Buying trait');
			viewModel.isLoading = true;

			userServices.buyTrait(traitId, authenticationServices.getUserDoc())
				.then(function(res) {
					viewModel.user.student.currency = res.data.currency;
					viewModel.character.inventory = res.data.inventory;
					$scope.$emit('itembought');
					viewModel.isLoading = false;
				})
				.catch(function(err) {
					console.log(err);
					viewModel.isLoading = false;
				})
		}

		function updateCharEquipedTraits() {

			// TODO: test update with achievements (populated change)
			// Set new values
			console.log('Updating char');
			var newChar = JSON.parse(JSON.stringify(viewModel.character));

			newChar.traits.body = parseBodyIds();
			newChar.traits.wearables = parseWearablesIds();

			newChar.traits.companion = null;
			newChar.inventory = viewModel.character.inventory;

			userServices.updateChar(newChar, authenticationServices.getUserDoc())
			.then(function(res) {
				// Store img on avatar URL and update user avatar
				viewModel.modelToWatch = {updateModel: true,  charTrait: viewModel.trait, generateAvatarURL: true};
				viewModel.equipEnabled = false;
				$scope.$emit('avatarupdated');
			})
			.catch(function(exc) {
				console.log('Error while trying to update character traits');
			});
		}

		function parseBodyIds() {

			var traitsBody = JSON.parse(JSON.stringify(viewModel.trait.body));

			for (var key in traitsBody) {
				if(traitsBody[key]) {
					traitsBody[key] = traitsBody[key]._id;
				}
			}

			return traitsBody;
		}

		function parseWearablesIds() {

			var traitsWearables = JSON.parse(JSON.stringify(viewModel.trait.wearables));

			for (var key in traitsWearables) {
				if(traitsWearables[key]) {
					traitsWearables[key] = traitsWearables[key]._id;
				}
			}

			return traitsWearables;
		}

	}]);
})();

/*character: {

		inventory: [{type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null}],

		traits: {
			body: {
				upperBody: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				lowerBody: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				eyes: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				mouth: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				hair: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				facialHair: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				colour: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null} -- > remove colour (it's on upperbody lowerbody and head)
			},

			wearables: {
				eyes: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				head: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				chest: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				legs: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				leftHand: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				rightHand: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
				feet: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null}
			},

			background: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null},
			companion: {type: mongoose.Schema.Types.ObjectId, ref: 'Trait', default:null}
		},


			xp: {type: Number, default: 0},

			achievements: [{
				achievement: {type: mongoose.Schema.Types.ObjectId, ref: 'Achievement', default: null},
				timestamp: {type:Date, default: new Date()}
			}],

			events: [{type: mongoose.Schema.Types.ObjectId, ref: 'Event', default: null}]
	},
*/