(function () {
    angular.module('griffinStudents.charCanvas')
        .factory('charCanvasServices', ['$rootScope', 'apiServices', 'errorHandler',
            function ($rootScope, apiServices, errorHandler) {

                var CharCanvasServices = {

                    getDefaultTrait: function (gender) {

                        var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/traits/default/find/gender/' + gender;
                        var req = {
                            method: 'GET',
                            url: _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
                    },
					
					getAllTraits: function() {
						
						var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/traits/all';
                        
						var req = {
                            method: 'GET',
                            url: _baseURL + _apiRoute,
                            headers: {}
                        };

                        return apiServices.apiRequest(req);
					},
					
					getSingleTrait: function(traitName) {
						
						var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                        var _apiRoute = '/traits/query';
                        
						var req = {
                            method: 'POST',
                            url: _baseURL + _apiRoute,
                            headers: {
								'Content-Type': 'application/json'
							},
							data: { 
								name: traitName
							}
                        };

                        return apiServices.apiRequest(req);
					},
					
					getTraits: function(searchText, filter, limit, page) {
                
						// Default values
						limit = limit || 5;
						page = page || 1;
					
						var req = {
							method: 'POST',
							url:  $rootScope.gfConfig.APIEndpoint + '/api/v1/traits/paginate',
							headers: {
								'Content-Type': 'application/json'
							},
							data: { 
								"searchField" : searchText,
								"limit" : limit,
								"page": page,
								"searchFilters": filter
							}
						};
						
						return apiServices.apiRequest(req);
					},

					getTraitsVersion: function() {
						var req = {
							method: 'GET',
							url:  $rootScope.gfConfig.APIEndpoint + '/api/v1/traits/version',
							headers: {},

						};

						return apiServices.apiRequest(req);
					}

                };


                return CharCanvasServices;
            }]);

})();