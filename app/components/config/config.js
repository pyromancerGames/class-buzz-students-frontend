(function () {
    angular.module('griffinStudents.config', [])

        .run(['$rootScope', function ($rootScope) {

            $rootScope.gfConfig = {

                APIEndpoint1: "http://192.168.1.100:5000",
                APIEndpoint: "http://ec2-54-94-155-99.sa-east-1.compute.amazonaws.com:5000",
                APIVersion: "/api/v1",
                AppKey: "6c1398ae5bb01f52d3ae00bc6a683c3beb129959",
                PublicAPIs: ["/student/create", "/login", "/student/signup"],
                S3Base: "http://pyromancer.co.gg.images.s3.amazonaws.com/",
                DefaultAvatarURL: "images/avatar-placeholder-01.png",
                DefaultCustomerLogoURL: "images/logo-placeholder.png"
            }

        }]);
})();