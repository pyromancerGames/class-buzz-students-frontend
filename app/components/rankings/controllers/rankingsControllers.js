angular.module('griffinStudents.rankings')

    .controller('RankingsCtrl', ['$rootScope', 'authenticationServices', 'rankingsServices', 'errorHandler', '_',
        function ($rootScope, authenticationServices, rankingsServices, errorHandler, _) {

            var viewModel = this;
            viewModel.houses = {};
            getHouses();

            $rootScope.$on('newevent', function(event, cbEvent){
                getHouses();
            });


            function getHouses() {
                rankingsServices.getRankings(authenticationServices.getProfile().customer)
                    .then( function(houses) {

                        viewModel.houses = _.sortBy(houses.data, '-score');
                        console.log(viewModel.houses);
                    })
                    .catch(function(err){ errorHandler.handleError(err); })
            }


        }]);

