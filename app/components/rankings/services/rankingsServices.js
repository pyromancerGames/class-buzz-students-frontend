(function () {
    angular.module('griffinStudents.rankings')

        .factory('rankingsServices', ['$rootScope', '$state', 'apiServices', function ($rootScope, $state, apiServices) {

            var Services = {

                getRankings: function (customerId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/houses/find/byCustomer';
                    var _data = { customer: customerId };

                    var req = {
                        method: 'POST',
                        url: _baseURL + _apiRoute,
                        headers: {},
                        data: _data
                    };

                    return apiServices.apiRequest(req);
                }


            }

            return Services;
        }]);

})();