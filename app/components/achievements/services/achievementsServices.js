(function () {
    angular.module('griffinStudents.achievements')

        .factory('achievementsServices', [ '$rootScope', '$state', 'apiServices', function ($rootScope, $state, apiServices) {

            var Services = {

                getCustomerAchievements: function (customerId) {

                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/achievements/find/byCustomer';
                    var _data = { customer: customerId };

                    var req = {
                        method: 'POST',
                        url: _baseURL + _apiRoute,
                        headers: {},
                        data: _data
                    };

                    return apiServices.apiRequest(req);
                },

                getAchievement: function(achievementId) {
                    var _baseURL = $rootScope.gfConfig.APIEndpoint + $rootScope.gfConfig.APIVersion;
                    var _apiRoute = '/achievements/' + achievementId;


                    var req = {
                        method: 'GET',
                        url: _baseURL + _apiRoute,
                        headers: {}
                    };

                    return apiServices.apiRequest(req);
                }
            }

            return Services;
        }]);

})();/**
 * Created by Daniel on 1/5/16.
 */
