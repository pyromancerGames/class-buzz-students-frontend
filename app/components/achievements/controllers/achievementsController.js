angular.module('griffinStudents.achievements')

    .controller('AchievementsCtrl', ['$rootScope', 'authenticationServices', 'userServices', 'errorHandler', 'achievementsServices', '_',
        function ($rootScope, authenticationServices, userServices, errorHandler, achievementsServices, _) {

            var viewModel = this;
            viewModel.userProfile = authenticationServices.getProfile();
            viewModel.countUserAchievements = countUserAchievements;
            viewModel.userAchievements = {};
            viewModel.customerAchievements = {};
            viewModel.distinctAchievementsReceived = 0;

            populateAchievements();

            function populateAchievements(){

                userServices.getUserAchievements(viewModel.userProfile)
                    .then(function (achievements) {
                        viewModel.userAchievements.data = achievements.data;

                        var uniqueList = _.uniq(viewModel.userAchievements.data, function(item){
                            return item.achievement;
                        });

                        uniqueList = _.filter(uniqueList,function(a){
                            return a.achievement.type == 'reward';
                        });

                        var idCount = _.countBy(uniqueList, function(item){
                            return item.achievement._id;
                        });

                        viewModel.userAchievements.idRecount = idCount;
                        viewModel.distinctAchievementsReceived = Object.keys(idCount).length;

                        return achievementsServices.getCustomerAchievements(viewModel.userProfile.customer);
                    })
                    .then(function (achievements) {

                        viewModel.customerAchievements.data = _.sortBy(achievements.data, function(a){
                            return countUserAchievements(a._id);
                        }).slice().reverse();

                        var typeCount = _.countBy(viewModel.customerAchievements.data, function(item){
                            return item.type;
                        });

                        var idCount = _.countBy(viewModel.customerAchievements.data, function(item){
                            return item._id;
                        });

                        viewModel.customerAchievements.typeRecount = typeCount;
                        viewModel.customerAchievements.idRecount = idCount;

                    })
                    .catch(function (err) {
                        errorHandler.handleError(err);
                    })
            }

            function countUserAchievements(achievementId){

                if (achievementId in viewModel.userAchievements.idRecount)
                    return viewModel.userAchievements.idRecount[achievementId];
                else
                    return 0;
            }

        }]);
