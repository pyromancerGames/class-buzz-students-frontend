'use strict';
(function() {
    /**
    * @ngdoc function
    * @name griffinApp.api
    * @description
    * Services of the griffinApp API
    */
    angular.module('griffinStudents.api')
    .factory('apiServices', ['$http', '$rootScope', '$q', '$location', 'authenticationServices', '_', function ($http, $rootScope, $q, $location, authenticationServices, _) {
        
        var APIServices = {

            apiRequest: function(req) {
                req.headers.appKey = $rootScope.gfConfig.AppKey;
                req.headers.Authorization = 'Bearer ' + authenticationServices.getToken();

                return $http(req);
            }
        }
        
        return APIServices;     
        
    }]);

})();