'use strict';

(function() {
    angular.module('griffinStudents.utils')
        .directive("preventDefault", function ()
        {
            var linkFn = function (scope, element, attrs) {
                element.on("click", function (event){
                    event.preventDefault();
                });
            };


            return {
                restrict: 'A',
                link: linkFn
            }

        })

        .directive("pwCheck", function() {
            return {
                require: "ngModel",
                scope: {
                    pw1: '='
                },
                link: function(scope, element, attrs, ctrl) {
                    scope.$watch(function() {
                        var combined;

                        if (scope.pw1 || ctrl.$viewValue) {
                            combined = scope.pw1 + '_' + ctrl.$viewValue;
                        }

                        return combined;

                    }, function(value) {
                        if (value) {

                            var pws = value.split('_');
                            if (pws[0] !== pws[1]) {
                                ctrl.$setValidity("pwmatch", false);
                                return undefined;
                            } else {
                                ctrl.$setValidity("pwmatch", true);
                                return pws[1];
                            }

                        }
                    });
                }
            };
        });


})();
