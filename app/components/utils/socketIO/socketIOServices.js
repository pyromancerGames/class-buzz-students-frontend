(function () {
    angular.module('griffinStudents.utils')

        .factory('socketioServices', ['$rootScope', '$socketio', 'authenticationServices',
            function ($rootScope, $socketio, authenticationServices) {
            var Services = {

                initSocketIOEvents: function (){
                    console.info("Initializing socketIO Events");

                    $socketio.emit("tieandjoin", authenticationServices.userProfile.id, authenticationServices.userProfile.customer);

                    $socketio.on('newnotification', function (notification) {
                        $rootScope.$broadcast('newnotification', notification);
                    });

                    $socketio.on('newquestavailable', function (newQuest) {
                        $rootScope.$broadcast('newquestavailable', newQuest);
                    });

                    $socketio.on('newchatmessage', function (newChat) {
                        $rootScope.$broadcast('newchatmessage', newChat);
                    });

                    $socketio.on('questcompleted', function (quest) {
                        $rootScope.$broadcast('questcompleted', quest);
                    });

                    $socketio.on('achievementaward', function (achievement) {
                        $rootScope.$broadcast('achievementaward', achievement);
                    });

                    $socketio.on('newEvent', function (event) {
                        $rootScope.$broadcast('newevent', event);
                    });
                }

            }
            return Services;

        }])

})();
