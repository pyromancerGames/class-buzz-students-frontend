(function () {
    angular.module('griffinStudents.utils')

        .factory('$socketio', ['$rootScope', 'socketFactory', function ($rootScope, socketFactory) {
            var myIoSocket = io.connect($rootScope.gfConfig.APIEndpoint);

            var mySocket = socketFactory({
                ioSocket: myIoSocket
            });

            return mySocket;

        }]);

})();
